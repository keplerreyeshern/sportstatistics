<?php

namespace App\Repository;

use App\Entity\GolesDate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method GolesDate|null find($id, $lockMode = null, $lockVersion = null)
 * @method GolesDate|null findOneBy(array $criteria, array $orderBy = null)
 * @method GolesDate[]    findAll()
 * @method GolesDate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GolesDateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GolesDate::class);
    }

    // /**
    //  * @return GolesDate[] Returns an array of GolesDate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GolesDate
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
