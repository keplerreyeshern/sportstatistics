<?php

namespace App\Repository;

use App\Entity\SoccerPositions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SoccerPositions|null find($id, $lockMode = null, $lockVersion = null)
 * @method SoccerPositions|null findOneBy(array $criteria, array $orderBy = null)
 * @method SoccerPositions[]    findAll()
 * @method SoccerPositions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SoccerPositionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SoccerPositions::class);
    }

    // /**
    //  * @return SoccerPositions[] Returns an array of SoccerPositions objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SoccerPositions
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
