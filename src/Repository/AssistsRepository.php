<?php

namespace App\Repository;

use App\Entity\Assists;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Assists|null find($id, $lockMode = null, $lockVersion = null)
 * @method Assists|null findOneBy(array $criteria, array $orderBy = null)
 * @method Assists[]    findAll()
 * @method Assists[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AssistsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Assists::class);
    }

    // /**
    //  * @return Assists[] Returns an array of Assists objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Assists
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
