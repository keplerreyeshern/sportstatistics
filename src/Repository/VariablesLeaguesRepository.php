<?php

namespace App\Repository;

use App\Entity\VariablesLeagues;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method VariablesLeagues|null find($id, $lockMode = null, $lockVersion = null)
 * @method VariablesLeagues|null findOneBy(array $criteria, array $orderBy = null)
 * @method VariablesLeagues[]    findAll()
 * @method VariablesLeagues[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VariablesLeaguesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VariablesLeagues::class);
    }

    // /**
    //  * @return VariablesLeagues[] Returns an array of VariablesLeagues objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VariablesLeagues
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
