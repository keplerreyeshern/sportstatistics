<?php

namespace App\Repository;

use App\Entity\Lineups;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Lineups|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lineups|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lineups[]    findAll()
 * @method Lineups[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LineupsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lineups::class);
    }

    // /**
    //  * @return Lineups[] Returns an array of Lineups objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lineups
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
