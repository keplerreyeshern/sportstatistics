<?php

namespace App\Repository;

use App\Entity\Faults;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Faults|null find($id, $lockMode = null, $lockVersion = null)
 * @method Faults|null findOneBy(array $criteria, array $orderBy = null)
 * @method Faults[]    findAll()
 * @method Faults[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FaultsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Faults::class);
    }

    // /**
    //  * @return Faults[] Returns an array of Faults objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Faults
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
