<?php

namespace App\Repository;

use App\Entity\PredictionResults;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PredictionResults|null find($id, $lockMode = null, $lockVersion = null)
 * @method PredictionResults|null findOneBy(array $criteria, array $orderBy = null)
 * @method PredictionResults[]    findAll()
 * @method PredictionResults[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PredictionResultsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PredictionResults::class);
    }

    // /**
    //  * @return PredictionResults[] Returns an array of PredictionResults objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PredictionResults
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
