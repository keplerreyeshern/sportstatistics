<?php

namespace App\Repository;

use App\Entity\PositionsTable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PositionsTable|null find($id, $lockMode = null, $lockVersion = null)
 * @method PositionsTable|null findOneBy(array $criteria, array $orderBy = null)
 * @method PositionsTable[]    findAll()
 * @method PositionsTable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PositionsTableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PositionsTable::class);
    }

    // /**
    //  * @return PositionsTable[] Returns an array of PositionsTable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PositionsTable
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
