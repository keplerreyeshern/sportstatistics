<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200220170749 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE goles_date ADD league_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE goles_date ADD CONSTRAINT FK_B18E20AF58AFC4DE FOREIGN KEY (league_id) REFERENCES leagues (id)');
        $this->addSql('CREATE INDEX IDX_B18E20AF58AFC4DE ON goles_date (league_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE goles_date DROP FOREIGN KEY FK_B18E20AF58AFC4DE');
        $this->addSql('DROP INDEX IDX_B18E20AF58AFC4DE ON goles_date');
        $this->addSql('ALTER TABLE goles_date DROP league_id');
    }
}
