<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200220173453 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE players DROP FOREIGN KEY FK_264E43A68642ED32');
        $this->addSql('DROP INDEX IDX_264E43A68642ED32 ON players');
        $this->addSql('ALTER TABLE players CHANGE leagues_id league_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE players ADD CONSTRAINT FK_264E43A658AFC4DE FOREIGN KEY (league_id) REFERENCES leagues (id)');
        $this->addSql('CREATE INDEX IDX_264E43A658AFC4DE ON players (league_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE players DROP FOREIGN KEY FK_264E43A658AFC4DE');
        $this->addSql('DROP INDEX IDX_264E43A658AFC4DE ON players');
        $this->addSql('ALTER TABLE players CHANGE league_id leagues_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE players ADD CONSTRAINT FK_264E43A68642ED32 FOREIGN KEY (leagues_id) REFERENCES leagues (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_264E43A68642ED32 ON players (leagues_id)');
    }
}
