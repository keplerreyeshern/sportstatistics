<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TeamsRepository")
 * @Vich\Uploadable
 */
class Teams
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $logo;

    /**
     * @Vich\UploadableField(mapping="logos", fileNameProperty="logo")
     * @var File
     */
    private $logoFile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Calendars", mappedBy="teamLocal")
     */
    private $calendars;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Calendars", mappedBy="teamVisitor")
     */
    private $calendarsv;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Leagues", inversedBy="teams")
     */
    private $league;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GolesDate", mappedBy="team")
     */
    private $golesDates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GolesTimes", mappedBy="team")
     */
    private $golesTimes;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Players", mappedBy="team")
     */
    private $players;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Assists", mappedBy="team")
     */
    private $assists;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stadiums", inversedBy="teams")
     */
    private $stadium;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FreeShots", mappedBy="team")
     */
    private $freeShots;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Penalties", mappedBy="team")
     */
    private $penalties;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Faults", mappedBy="committedTeam")
     */
    private $faults;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CornerKicks", mappedBy="team")
     */
    private $cornerKicks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Shooting", mappedBy="team")
     */
    private $shootings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offsides", mappedBy="team")
     */
    private $offsides;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    public function __construct()
    {
        $this->status = true;
        $this->calendars = new ArrayCollection();
        $this->date = new ArrayCollection();
        $this->golesDates = new ArrayCollection();
        $this->golesTimes = new ArrayCollection();
        $this->players = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->assists = new ArrayCollection();
        $this->freeShots = new ArrayCollection();
        $this->penalties = new ArrayCollection();
        $this->faults = new ArrayCollection();
        $this->cornerKicks = new ArrayCollection();
        $this->shootings = new ArrayCollection();
        $this->offsides = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }


    public function setLogoFile(File $logo = null)
    {
        $this->logoFile = $logo;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($logo) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getLogoFile()
    {
        return $this->logoFile;
    }

    /**
     * @return Collection|Calendars[]
     */
    public function getCalendars(): Collection
    {
        return $this->calendars;
    }

    public function addCalendar(Calendars $calendar): self
    {
        if (!$this->calendars->contains($calendar)) {
            $this->calendars[] = $calendar;
            $calendar->setTeamLocal($this);
        }

        return $this;
    }

    public function removeCalendar(Calendars $calendar): self
    {
        if ($this->calendars->contains($calendar)) {
            $this->calendars->removeElement($calendar);
            // set the owning side to null (unless already changed)
            if ($calendar->getTeamLocal() === $this) {
                $calendar->setTeamLocal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Calendars[]
     */
    public function getCalendarsv(): Collection
    {
        return $this->calendarsv;
    }

    public function addCalendars(Calendars $calendarsv): self
    {
        if (!$this->calendarsv->contains($calendarsv)) {
            $this->calendarsv[] = $calendarsv;
            $calendarsv->setTeamVisitor($this);
        }

        return $this;
    }

    public function removeCalendarsv(Calendars $calendarsv): self
    {
        if ($this->calendarsv->contains($calendarsv)) {
            $this->calendarsv->removeElement($calendarsv);
            // set the owning side to null (unless already changed)
            if ($calendarsv->getTeamVisitor() === $this) {
                $calendarsv->setTeamVisitor(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->name;
    }


    public function getLeague(): ?Leagues
    {
        return $this->league;
    }

    public function setLeague(?Leagues $league): self
    {
        $this->league = $league;

        return $this;
    }

    /**
     * @return Collection|GamesResults[]
     */
    public function getGamesResults(): Collection
    {
        return $this->gamesResults;
    }

    public function addGamesResult(GamesResults $gamesResult): self
    {
        if (!$this->gamesResults->contains($gamesResult)) {
            $this->gamesResults[] = $gamesResult;
            $gamesResult->setTeamLocal($this);
        }

        return $this;
    }

    public function removeGamesResult(GamesResults $gamesResult): self
    {
        if ($this->gamesResults->contains($gamesResult)) {
            $this->gamesResults->removeElement($gamesResult);
            // set the owning side to null (unless already changed)
            if ($gamesResult->getTeamLocal() === $this) {
                $gamesResult->setTeamLocal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GolesDate[]
     */
    public function getGolesDates(): Collection
    {
        return $this->golesDates;
    }

    public function addGolesDate(GolesDate $golesDate): self
    {
        if (!$this->golesDates->contains($golesDate)) {
            $this->golesDates[] = $golesDate;
            $golesDate->setTeam($this);
        }

        return $this;
    }

    public function removeGolesDate(GolesDate $golesDate): self
    {
        if ($this->golesDates->contains($golesDate)) {
            $this->golesDates->removeElement($golesDate);
            // set the owning side to null (unless already changed)
            if ($golesDate->getTeam() === $this) {
                $golesDate->setTeam(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GolesTimes[]
     */
    public function getGolesTimes(): Collection
    {
        return $this->golesTimes;
    }

    public function addGolesTime(GolesTimes $golesTime): self
    {
        if (!$this->golesTimes->contains($golesTime)) {
            $this->golesTimes[] = $golesTime;
            $golesTime->setTeam($this);
        }

        return $this;
    }

    public function removeGolesTime(GolesTimes $golesTime): self
    {
        if ($this->golesTimes->contains($golesTime)) {
            $this->golesTimes->removeElement($golesTime);
            // set the owning side to null (unless already changed)
            if ($golesTime->getTeam() === $this) {
                $golesTime->setTeam(null);
            }
        }

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|Players[]
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    public function addPlayer(Players $player): self
    {
        if (!$this->players->contains($player)) {
            $this->players[] = $player;
            $player->setTeam($this);
        }

        return $this;
    }

    public function removePlayer(Players $player): self
    {
        if ($this->players->contains($player)) {
            $this->players->removeElement($player);
            // set the owning side to null (unless already changed)
            if ($player->getTeam() === $this) {
                $player->setTeam(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Assists[]
     */
    public function getAssists(): Collection
    {
        return $this->assists;
    }

    public function addAssist(Assists $assist): self
    {
        if (!$this->assists->contains($assist)) {
            $this->assists[] = $assist;
            $assist->setTeam($this);
        }

        return $this;
    }

    public function removeAssist(Assists $assist): self
    {
        if ($this->assists->contains($assist)) {
            $this->assists->removeElement($assist);
            // set the owning side to null (unless already changed)
            if ($assist->getTeam() === $this) {
                $assist->setTeam(null);
            }
        }

        return $this;
    }

    public function getStadium(): ?Stadiums
    {
        return $this->stadium;
    }

    public function setStadium(?Stadiums $stadium): self
    {
        $this->stadium = $stadium;

        return $this;
    }

    /**
     * @return Collection|FreeShots[]
     */
    public function getFreeShots(): Collection
    {
        return $this->freeShots;
    }

    public function addFreeShot(FreeShots $freeShot): self
    {
        if (!$this->freeShots->contains($freeShot)) {
            $this->freeShots[] = $freeShot;
            $freeShot->setTeam($this);
        }

        return $this;
    }

    public function removeFreeShot(FreeShots $freeShot): self
    {
        if ($this->freeShots->contains($freeShot)) {
            $this->freeShots->removeElement($freeShot);
            // set the owning side to null (unless already changed)
            if ($freeShot->getTeam() === $this) {
                $freeShot->setTeam(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Penalties[]
     */
    public function getPenalties(): Collection
    {
        return $this->penalties;
    }

    public function addPenalty(Penalties $penalty): self
    {
        if (!$this->penalties->contains($penalty)) {
            $this->penalties[] = $penalty;
            $penalty->setTeam($this);
        }

        return $this;
    }

    public function removePenalty(Penalties $penalty): self
    {
        if ($this->penalties->contains($penalty)) {
            $this->penalties->removeElement($penalty);
            // set the owning side to null (unless already changed)
            if ($penalty->getTeam() === $this) {
                $penalty->setTeam(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Faults[]
     */
    public function getFaults(): Collection
    {
        return $this->faults;
    }

    public function addFault(Faults $fault): self
    {
        if (!$this->faults->contains($fault)) {
            $this->faults[] = $fault;
            $fault->setCommittedTeam($this);
        }

        return $this;
    }

    public function removeFault(Faults $fault): self
    {
        if ($this->faults->contains($fault)) {
            $this->faults->removeElement($fault);
            // set the owning side to null (unless already changed)
            if ($fault->getCommittedTeam() === $this) {
                $fault->setCommittedTeam(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CornerKicks[]
     */
    public function getCornerKicks(): Collection
    {
        return $this->cornerKicks;
    }

    public function addCornerKick(CornerKicks $cornerKick): self
    {
        if (!$this->cornerKicks->contains($cornerKick)) {
            $this->cornerKicks[] = $cornerKick;
            $cornerKick->setTeam($this);
        }

        return $this;
    }

    public function removeCornerKick(CornerKicks $cornerKick): self
    {
        if ($this->cornerKicks->contains($cornerKick)) {
            $this->cornerKicks->removeElement($cornerKick);
            // set the owning side to null (unless already changed)
            if ($cornerKick->getTeam() === $this) {
                $cornerKick->setTeam(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Shooting[]
     */
    public function getShootings(): Collection
    {
        return $this->shootings;
    }

    public function addShooting(Shooting $shooting): self
    {
        if (!$this->shootings->contains($shooting)) {
            $this->shootings[] = $shooting;
            $shooting->setTeam($this);
        }

        return $this;
    }

    public function removeShooting(Shooting $shooting): self
    {
        if ($this->shootings->contains($shooting)) {
            $this->shootings->removeElement($shooting);
            // set the owning side to null (unless already changed)
            if ($shooting->getTeam() === $this) {
                $shooting->setTeam(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Offsides[]
     */
    public function getOffsides(): Collection
    {
        return $this->offsides;
    }

    public function addOffside(Offsides $offside): self
    {
        if (!$this->offsides->contains($offside)) {
            $this->offsides[] = $offside;
            $offside->setTeam($this);
        }

        return $this;
    }

    public function removeOffside(Offsides $offside): self
    {
        if ($this->offsides->contains($offside)) {
            $this->offsides->removeElement($offside);
            // set the owning side to null (unless already changed)
            if ($offside->getTeam() === $this) {
                $offside->setTeam(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

}
