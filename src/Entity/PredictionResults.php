<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PredictionResultsRepository")
 */
class PredictionResults
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Calendars", inversedBy="predictionResults")
     */
    private $game;

    /**
     * @ORM\Column(type="boolean")
     */
    private $success;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame(): ?Calendars
    {
        return $this->game;
    }

    public function setGame(?Calendars $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getSuccess(): ?bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }
}
