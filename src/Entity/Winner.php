<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WinnerRepository")
 */
class Winner
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teams", inversedBy="winners")
     */
    private $ten;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teams", inversedBy="winners")
     */
    private $thirty;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teams", inversedBy="winners")
     */
    private $firstTime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teams", inversedBy="winners")
     */
    private $end;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Calendars", cascade={"persist", "remove"})
     */
    private $game;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->updatedAt = new \DateTime();
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTen(): ?Teams
    {
        return $this->ten;
    }

    public function setTen(?Teams $ten): self
    {
        $this->ten = $ten;

        return $this;
    }

    public function getThirty(): ?Teams
    {
        return $this->thirty;
    }

    public function setThirty(?Teams $thirty): self
    {
        $this->thirty = $thirty;

        return $this;
    }

    public function getFirstTime(): ?Teams
    {
        return $this->firstTime;
    }

    public function setFirstTime(?Teams $firstTime): self
    {
        $this->firstTime = $firstTime;

        return $this;
    }

    public function getEnd(): ?Teams
    {
        return $this->end;
    }

    public function setEnd(?Teams $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getGame(): ?Calendars
    {
        return $this->game;
    }

    public function setGame(?Calendars $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
