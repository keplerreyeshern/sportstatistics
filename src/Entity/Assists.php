<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AssistsRepository")
 */
class Assists
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="assists")
     */
    private $player;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teams", inversedBy="assists")
     */
    private $team;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Calendars", inversedBy="assists")
     */
    private $game;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Leagues", inversedBy="assists")
     */
    private $league;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minute;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlayer(): ?Players
    {
        return $this->player;
    }

    public function setPlayer(?Players $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getTeam(): ?Teams
    {
        return $this->team;
    }

    public function setTeam(?Teams $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getGame(): ?Calendars
    {
        return $this->game;
    }

    public function setGame(?Calendars $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getLeague(): ?Leagues
    {
        return $this->league;
    }

    public function setLeague(?Leagues $league): self
    {
        $this->league = $league;

        return $this;
    }

    public function getMinute(): ?int
    {
        return $this->minute;
    }

    public function setMinute(int $minute): self
    {
        $this->minute = $minute;

        return $this;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->player. ' -> '.$this->team;
    }
}
