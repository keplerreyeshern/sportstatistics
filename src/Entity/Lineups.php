<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LineupsRepository")
 */
class Lineups
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Calendars", inversedBy="lineups")
     */
    private $game;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $alignmentLocal;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $alignmentVisitor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $defending1Local;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $defending2Local;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $defending3Local;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $defending4Local;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $defending1Visitor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $defending2Visitor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $defending3Visitor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $defending4Visitor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $midfielder1Local;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $midfielder2Local;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $midfielder3Local;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $midfielder4Local;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $midfielder1Visitor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $midfielder2Visitor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $midfielder3Visitor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $midfielder4Visitor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $forward1Local;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $forward2Local;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $forward3Local;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $forward4Local;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $forward1Visitor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $forward2Visitor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $forward3Visitor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $forward4Visitor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $goalkeeperLocal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $goalkeeperVisitor;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $alternatesLocal1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $alternatesLocal2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $alternatesLocal3;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $alternatesLocal4;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $alternatesLocal5;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $alternatesLocal6;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $alternatesVisitor1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $alternatesVisitor2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $alternatesVisitor3;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $alternatesVisitor4;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $alternatesVisitor5;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="lineups")
     */
    private $alternatesVisitor6;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame(): ?Calendars
    {
        return $this->game;
    }

    public function setGame(?Calendars $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getAlignmentLocal(): ?string
    {
        return $this->alignmentLocal;
    }

    public function setAlignmentLocal(string $alignmentLocal): self
    {
        $this->alignmentLocal = $alignmentLocal;

        return $this;
    }

    public function getAlignmentVisitor(): ?string
    {
        return $this->alignmentVisitor;
    }

    public function setAlignmentVisitor(string $alignmentVisitor): self
    {
        $this->alignmentVisitor = $alignmentVisitor;

        return $this;
    }

    public function getDefending1Local(): ?Players
    {
        return $this->defending1Local;
    }

    public function setDefending1Local(?Players $defending1Local): self
    {
        $this->defending1Local = $defending1Local;

        return $this;
    }

    public function getDefending2Local(): ?Players
    {
        return $this->defending2Local;
    }

    public function setDefending2Local(?Players $defending2Local): self
    {
        $this->defending2Local = $defending2Local;

        return $this;
    }

    public function getDefending3Local(): ?Players
    {
        return $this->defending3Local;
    }

    public function setDefending3Local(?Players $defending3Local): self
    {
        $this->defending3Local = $defending3Local;

        return $this;
    }

    public function getDefending4Local(): ?Players
    {
        return $this->defending4Local;
    }

    public function setDefending4Local(?Players $defending4Local): self
    {
        $this->defending4Local = $defending4Local;

        return $this;
    }

    public function getDefending1Visitor(): ?Players
    {
        return $this->defending1Visitor;
    }

    public function setDefending1Visitor(?Players $defending1Visitor): self
    {
        $this->defending1Visitor = $defending1Visitor;

        return $this;
    }

    public function getDefending2Visitor(): ?Players
    {
        return $this->defending2Visitor;
    }

    public function setDefending2Visitor(?Players $defending2Visitor): self
    {
        $this->defending2Visitor = $defending2Visitor;

        return $this;
    }

    public function getDefending3Visitor(): ?Players
    {
        return $this->defending3Visitor;
    }

    public function setDefending3Visitor(?Players $defending3Visitor): self
    {
        $this->defending3Visitor = $defending3Visitor;

        return $this;
    }

    public function getDefending4Visitor(): ?Players
    {
        return $this->defending4Visitor;
    }

    public function setDefending4Visitor(?Players $defending4Visitor): self
    {
        $this->defending4Visitor = $defending4Visitor;

        return $this;
    }

    public function getMidfielder1Local(): ?Players
    {
        return $this->midfielder1Local;
    }

    public function setMidfielder1Local(?Players $midfielder1Local): self
    {
        $this->midfielder1Local = $midfielder1Local;

        return $this;
    }

    public function getMidfielder2Local(): ?Players
    {
        return $this->midfielder2Local;
    }

    public function setMidfielder2Local(?Players $midfielder2Local): self
    {
        $this->midfielder2Local = $midfielder2Local;

        return $this;
    }

    public function getMidfielder3Local(): ?Players
    {
        return $this->midfielder3Local;
    }

    public function setMidfielder3Local(?Players $midfielder3Local): self
    {
        $this->midfielder3Local = $midfielder3Local;

        return $this;
    }

    public function getMidfielder4Local(): ?Players
    {
        return $this->midfielder4Local;
    }

    public function setMidfielder4Local(?Players $midfielder4Local): self
    {
        $this->midfielder4Local = $midfielder4Local;

        return $this;
    }

    public function getMidfielder1Visitor(): ?Players
    {
        return $this->midfielder1Visitor;
    }

    public function setMidfielder1Visitor(?Players $midfielder1Visitor): self
    {
        $this->midfielder1Visitor = $midfielder1Visitor;

        return $this;
    }

    public function getMidfielder2Visitor(): ?Players
    {
        return $this->midfielder2Visitor;
    }

    public function setMidfielder2Visitor(?Players $midfielder2Visitor): self
    {
        $this->midfielder2Visitor = $midfielder2Visitor;

        return $this;
    }

    public function getMidfielder3Visitor(): ?Players
    {
        return $this->midfielder3Visitor;
    }

    public function setMidfielder3Visitor(?Players $midfielder3Visitor): self
    {
        $this->midfielder3Visitor = $midfielder3Visitor;

        return $this;
    }

    public function getMidfielder4Visitor(): ?Players
    {
        return $this->midfielder4Visitor;
    }

    public function setMidfielder4Visitor(?Players $midfielder4Visitor): self
    {
        $this->midfielder4Visitor = $midfielder4Visitor;

        return $this;
    }

    public function getForward1Local(): ?Players
    {
        return $this->forward1Local;
    }

    public function setForward1Local(?Players $forward1Local): self
    {
        $this->forward1Local = $forward1Local;

        return $this;
    }

    public function getForward2Local(): ?Players
    {
        return $this->forward2Local;
    }

    public function setForward2Local(?Players $forward2Local): self
    {
        $this->forward2Local = $forward2Local;

        return $this;
    }

    public function getForward3Local(): ?Players
    {
        return $this->forward3Local;
    }

    public function setForward3Local(?Players $forward3Local): self
    {
        $this->forward3Local = $forward3Local;

        return $this;
    }

    public function getForward4Local(): ?Players
    {
        return $this->forward4Local;
    }

    public function setForward4Local(?Players $forward4Local): self
    {
        $this->forward4Local = $forward4Local;

        return $this;
    }

    public function getForward1Visitor(): ?Players
    {
        return $this->forward1Visitor;
    }

    public function setForward1Visitor(?Players $forward1Visitor): self
    {
        $this->forward1Visitor = $forward1Visitor;

        return $this;
    }

    public function getForward2Visitor(): ?Players
    {
        return $this->forward2Visitor;
    }

    public function setForward2Visitor(?Players $forward2Visitor): self
    {
        $this->forward2Visitor = $forward2Visitor;

        return $this;
    }

    public function getForward3Visitor(): ?Players
    {
        return $this->forward3Visitor;
    }

    public function setForward3Visitor(?Players $forward3Visitor): self
    {
        $this->forward3Visitor = $forward3Visitor;

        return $this;
    }

    public function getForward4Visitor(): ?Players
    {
        return $this->forward4Visitor;
    }

    public function setForward4Visitor(?Players $forward4Visitor): self
    {
        $this->forward4Visitor = $forward4Visitor;

        return $this;
    }

    public function getGoalkeeperLocal(): ?Players
    {
        return $this->goalkeeperLocal;
    }

    public function setGoalkeeperLocal(?Players $goalkeeperLocal): self
    {
        $this->goalkeeperLocal = $goalkeeperLocal;

        return $this;
    }

    public function getGoalkeeperVisitor(): ?Players
    {
        return $this->goalkeeperVisitor;
    }

    public function setGoalkeeperVisitor(?Players $goalkeeperVisitor): self
    {
        $this->goalkeeperVisitor = $goalkeeperVisitor;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getAlternatesLocal1(): ?Players
    {
        return $this->alternatesLocal1;
    }

    public function setAlternatesLocal1(?Players $alternatesLocal1): self
    {
        $this->alternatesLocal1 = $alternatesLocal1;

        return $this;
    }

    public function getAlternatesLocal2(): ?Players
    {
        return $this->alternatesLocal2;
    }

    public function setAlternatesLocal2(?Players $alternatesLocal2): self
    {
        $this->alternatesLocal2 = $alternatesLocal2;

        return $this;
    }

    public function getAlternatesLocal3(): ?Players
    {
        return $this->alternatesLocal3;
    }

    public function setAlternatesLocal3(?Players $alternatesLocal3): self
    {
        $this->alternatesLocal3 = $alternatesLocal3;

        return $this;
    }

    public function getAlternatesLocal4(): ?Players
    {
        return $this->alternatesLocal4;
    }

    public function setAlternatesLocal4(?Players $alternatesLocal4): self
    {
        $this->alternatesLocal4 = $alternatesLocal4;

        return $this;
    }

    public function getAlternatesLocal5(): ?Players
    {
        return $this->alternatesLocal5;
    }

    public function setAlternatesLocal5(?Players $alternatesLocal5): self
    {
        $this->alternatesLocal5 = $alternatesLocal5;

        return $this;
    }

    public function getAlternatesLocal6(): ?Players
    {
        return $this->alternatesLocal6;
    }

    public function setAlternatesLocal6(?Players $alternatesLocal6): self
    {
        $this->alternatesLocal6 = $alternatesLocal6;

        return $this;
    }

    public function getAlternatesVisitor1(): ?Players
    {
        return $this->alternatesVisitor1;
    }

    public function setAlternatesVisitor1(?Players $alternatesVisitor1): self
    {
        $this->alternatesVisitor1 = $alternatesVisitor1;

        return $this;
    }

    public function getAlternatesVisitor2(): ?Players
    {
        return $this->alternatesVisitor2;
    }

    public function setAlternatesVisitor2(?Players $alternatesVisitor2): self
    {
        $this->alternatesVisitor2 = $alternatesVisitor2;

        return $this;
    }

    public function getAlternatesVisitor3(): ?Players
    {
        return $this->alternatesVisitor3;
    }

    public function setAlternatesVisitor3(?Players $alternatesVisitor3): self
    {
        $this->alternatesVisitor3 = $alternatesVisitor3;

        return $this;
    }

    public function getAlternatesVisitor4(): ?Players
    {
        return $this->alternatesVisitor4;
    }

    public function setAlternatesVisitor4(?Players $alternatesVisitor4): self
    {
        $this->alternatesVisitor4 = $alternatesVisitor4;

        return $this;
    }

    public function getAlternatesVisitor5(): ?Players
    {
        return $this->alternatesVisitor5;
    }

    public function setAlternatesVisitor5(?Players $alternatesVisitor5): self
    {
        $this->alternatesVisitor5 = $alternatesVisitor5;

        return $this;
    }

    public function getAlternatesVisitor6(): ?Players
    {
        return $this->alternatesVisitor6;
    }

    public function setAlternatesVisitor6(?Players $alternatesVisitor6): self
    {
        $this->alternatesVisitor6 = $alternatesVisitor6;

        return $this;
    }
}
