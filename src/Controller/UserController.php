<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/register", name="app_register", methods={"GET","POST"})
     */
    public function register(Request $request)
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){

        }
        $form = $form->createView();
        return $this->render('user/index.html.twig', compact('form'));
    }

    /**
     * @Route("/register/validation//username/{username}", name="app_validation_username", methods={"GET"})
     */
    public function validationUsername(Request $request)
    {
        $username = $this->getDoctrine()->getRepository(User::class)->findOneBy(['username' => $request->get('username')]);
        return $this->render('user/validation_username.html.twig', compact('username'));

    }
}
