<?php

namespace App\Controller;

use App\Entity\Calendars;
use App\Entity\GamesResults;
use App\Entity\Leagues;
use App\Entity\VariablesLeagues;
use App\Repository\GamesResultsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/forecasts")
 */
class ForecastsController extends AbstractController
{
    /**
     * @Route("/", name="app_forecasts")
     */
    public function index(GamesResultsRepository $gamesResultsRepository)
    {
        $active = 'forecasts';
        $league = $this->getDoctrine()
            ->getRepository(Leagues::class)
            ->findOneBy([
                'isActive' => true
            ]);
        $journey = $this->getDoctrine()
            ->getRepository(VariablesLeagues::class)
            ->findOneBy(['isActive' => true]);
        $journey = $journey->getJourney();
        $journey1 = $journey - 1;
        $gamesnexts = $this->getDoctrine()
            ->getRepository(Calendars::class)
            ->findBy([
                'journey' => $journey,
                'year' => date('Y'),
                'league' => $league->getID()]);
        return $this->render('forecasts/index.html.twig', compact('active', 'gamesnexts', 'gamesResultsRepository', 'journey1'));
    }

    /**
     * @Route("/{id}", name="show_picks")
     */
    public function show(Calendars $calendars)
    {
        $active = 'forecasts';
        $journey = $this->getDoctrine()->getRepository(VariablesLeagues::class)->findOneBy(['isActive' => true]);
        $journey = $journey->getJourney();
        $journey1 = $journey - 1;
        $averageWinLocal = $this->getDoctrine()->getRepository(GamesResults::class)->percentageToWin($calendars->getTeamLocal()->getId(), $journey1,1);
        $averageWinVisitor = $this->getDoctrine()->getRepository(GamesResults::class)->percentageToWin($calendars->getTeamVisitor()->getId(), $journey1,1);
        $statisticsTeamLocal = $this->getDoctrine()->getRepository(GamesResults::class)->statisticsForTeam($calendars->getTeamLocal()->getId(), $journey1, 1);
        $statisticsTeamVisitor = $this->getDoctrine()->getRepository(GamesResults::class)->statisticsForTeam($calendars->getTeamVisitor()->getId(), $journey1, 1);
        return $this->render('forecasts/show.html.twig', compact('active', 'calendars', 'statisticsTeamLocal', 'statisticsTeamVisitor', 'averageWinLocal', 'averageWinVisitor'));
    }
}
