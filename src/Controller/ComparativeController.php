<?php

namespace App\Controller;

use App\Entity\Calendars;
use App\Entity\GamesResults;
use App\Entity\VariablesLeagues;
use App\Repository\GamesResultsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/comparative")
 */
class ComparativeController extends AbstractController
{
    /**
     * @Route("/", name="comparative_index", methods={"GET"})
     */
    public function index(GamesResultsRepository $gamesResultsRepository):Response
    {
        $active = 'comparative';
        $journey = $this->getDoctrine()->getRepository(VariablesLeagues::class)->findOneBy(['isActive' => true]);
        $journey = $journey->getJourney() - 1;
        $lastsResults = $this->getDoctrine()->getRepository(GamesResults::class)->findBy([],['id' => 'ASC'], 9);
        $calendars = $this->getDoctrine()->getRepository(Calendars::class)->findBy(['journey' => $journey, 'year' => date('Y'), 'league' => 1], ['id' => 'ASC']);
        return $this->render('comparative/index.html.twig', compact('active', 'lastsResults', 'journey', 'calendars', 'gamesResultsRepository') );
    }
}
