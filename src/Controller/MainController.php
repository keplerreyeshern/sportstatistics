<?php

namespace App\Controller;

use App\Entity\Leagues;
use App\Entity\Players;
use App\Repository\CalendarsRepository;
use App\Repository\GamesResultsRepository;
use App\Repository\VariablesLeaguesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="app_index")
     */
    public function index(GamesResultsRepository $gamesResultsRepository, CalendarsRepository $calendarsRepository, VariablesLeaguesRepository $variablesLeaguesRepository)
    {
        $active = 'home';
        $league = $this->getDoctrine()->getRepository(Leagues::class)->findOneBy(['isActive' => true]);
        $journey = $variablesLeaguesRepository->findOneBy(['isActive' => true, 'league' => $league->getId()]);
        $journey = $journey->getJourney();
        $journey1 = $journey - 1;
        $resultlast = $gamesResultsRepository->lastResult(date('Y'), $journey1, $league->getId(), 1);
        $resultprueba = $gamesResultsRepository->findBy([], ['id' => 'DESC']);
        $gamenext = $calendarsRepository->findBy(['journey' => $journey, 'league' => $league->getId(), 'year' => date('Y')], ['id' => 'DESC'], 1);
        $gamesnexts = $calendarsRepository->findBy(['journey' => $journey, 'league' => $league->getId(),   'year' => date('Y')], ['id' => 'DESC'], 9);;
        $players = $this->getDoctrine()->getRepository(Players::class)->findAll();
        return $this->render('main/index.html.twig', compact('resultlast', 'active', 'gamenext', 'gamesnexts', 'gamesResultsRepository', 'journey1', 'players', 'resultprueba'));
    }
}
