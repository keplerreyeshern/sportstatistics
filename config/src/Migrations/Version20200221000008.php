<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200221000008 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE statistics_game (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, faults_local INT NOT NULL, faults_visitor INT NOT NULL, reds_local INT NOT NULL, reds_visitor INT NOT NULL, yellows_local INT NOT NULL, yellows_visitor INT NOT NULL, offside_local INT NOT NULL, offside_visitor INT NOT NULL, corners_local INT NOT NULL, corners_visitor INT NOT NULL, saved_local INT NOT NULL, saved_visitor INT NOT NULL, possession_local INT NOT NULL, possession_visitor INT NOT NULL, goal_shots_local INT NOT NULL, goal_shots_visitor INT NOT NULL, kicks_to_goal_local INT NOT NULL, kicks_to_goal_visitor INT NOT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_1CD016CEE48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lineups (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, defending1_local_id INT DEFAULT NULL, defending2_local_id INT DEFAULT NULL, defending3_local_id INT DEFAULT NULL, defending4_local_id INT DEFAULT NULL, defending1_visitor_id INT DEFAULT NULL, defending2_visitor_id INT DEFAULT NULL, defending3_visitor_id INT DEFAULT NULL, defending4_visitor_id INT DEFAULT NULL, midfielder1_local_id INT DEFAULT NULL, midfielder2_local_id INT DEFAULT NULL, midfielder3_local_id INT DEFAULT NULL, midfielder4_local_id INT DEFAULT NULL, midfielder1_visitor_id INT DEFAULT NULL, midfielder2_visitor_id INT DEFAULT NULL, midfielder3_visitor_id INT DEFAULT NULL, midfielder4_visitor_id INT DEFAULT NULL, forward1_local_id INT DEFAULT NULL, forward2_local_id INT DEFAULT NULL, forward3_local_id INT DEFAULT NULL, forward4_local_id INT DEFAULT NULL, forward1_visitor_id INT DEFAULT NULL, forward2_visitor_id INT DEFAULT NULL, forward3_visitor_id INT DEFAULT NULL, forward4_visitor_id INT DEFAULT NULL, goalkeeper_local_id INT DEFAULT NULL, goalkeeper_visitor_id INT DEFAULT NULL, alignment_local VARCHAR(255) NOT NULL, alignment_visitor VARCHAR(255) NOT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_60729AABE48FD905 (game_id), INDEX IDX_60729AABD5606FE7 (defending1_local_id), INDEX IDX_60729AAB3E57D4E4 (defending2_local_id), INDEX IDX_60729AABD195BFDA (defending3_local_id), INDEX IDX_60729AAB3349A4A3 (defending4_local_id), INDEX IDX_60729AAB9E4B5EAA (defending1_visitor_id), INDEX IDX_60729AABE9D58C5A (defending2_visitor_id), INDEX IDX_60729AAB7270C035 (defending3_visitor_id), INDEX IDX_60729AAB6E829BA (defending4_visitor_id), INDEX IDX_60729AABE2012A92 (midfielder1_local_id), INDEX IDX_60729AAB9369191 (midfielder2_local_id), INDEX IDX_60729AABE6F4FAAF (midfielder3_local_id), INDEX IDX_60729AAB428E1D6 (midfielder4_local_id), INDEX IDX_60729AABCAB55167 (midfielder1_visitor_id), INDEX IDX_60729AABBD2B8397 (midfielder2_visitor_id), INDEX IDX_60729AAB268ECFF8 (midfielder3_visitor_id), INDEX IDX_60729AAB52162677 (midfielder4_visitor_id), INDEX IDX_60729AAB6256E97D (forward1_local_id), INDEX IDX_60729AAB8961527E (forward2_local_id), INDEX IDX_60729AAB66A33940 (forward3_local_id), INDEX IDX_60729AAB847F2239 (forward4_local_id), INDEX IDX_60729AAB113EAD19 (forward1_visitor_id), INDEX IDX_60729AAB66A07FE9 (forward2_visitor_id), INDEX IDX_60729AABFD053386 (forward3_visitor_id), INDEX IDX_60729AAB899DDA09 (forward4_visitor_id), INDEX IDX_60729AAB65A083D2 (goalkeeper_local_id), INDEX IDX_60729AAB95C6828F (goalkeeper_visitor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE statistics_game ADD CONSTRAINT FK_1CD016CEE48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABE48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABD5606FE7 FOREIGN KEY (defending1_local_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB3E57D4E4 FOREIGN KEY (defending2_local_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABD195BFDA FOREIGN KEY (defending3_local_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB3349A4A3 FOREIGN KEY (defending4_local_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB9E4B5EAA FOREIGN KEY (defending1_visitor_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABE9D58C5A FOREIGN KEY (defending2_visitor_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB7270C035 FOREIGN KEY (defending3_visitor_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB6E829BA FOREIGN KEY (defending4_visitor_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABE2012A92 FOREIGN KEY (midfielder1_local_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB9369191 FOREIGN KEY (midfielder2_local_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABE6F4FAAF FOREIGN KEY (midfielder3_local_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB428E1D6 FOREIGN KEY (midfielder4_local_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABCAB55167 FOREIGN KEY (midfielder1_visitor_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABBD2B8397 FOREIGN KEY (midfielder2_visitor_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB268ECFF8 FOREIGN KEY (midfielder3_visitor_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB52162677 FOREIGN KEY (midfielder4_visitor_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB6256E97D FOREIGN KEY (forward1_local_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB8961527E FOREIGN KEY (forward2_local_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB66A33940 FOREIGN KEY (forward3_local_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB847F2239 FOREIGN KEY (forward4_local_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB113EAD19 FOREIGN KEY (forward1_visitor_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB66A07FE9 FOREIGN KEY (forward2_visitor_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABFD053386 FOREIGN KEY (forward3_visitor_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB899DDA09 FOREIGN KEY (forward4_visitor_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB65A083D2 FOREIGN KEY (goalkeeper_local_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB95C6828F FOREIGN KEY (goalkeeper_visitor_id) REFERENCES players (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE statistics_game');
        $this->addSql('DROP TABLE lineups');
    }
}
