<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200218213833 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE goles_date ADD player_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE goles_date ADD CONSTRAINT FK_B18E20AF99E6F5DF FOREIGN KEY (player_id) REFERENCES players (id)');
        $this->addSql('CREATE INDEX IDX_B18E20AF99E6F5DF ON goles_date (player_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE goles_date DROP FOREIGN KEY FK_B18E20AF99E6F5DF');
        $this->addSql('DROP INDEX IDX_B18E20AF99E6F5DF ON goles_date');
        $this->addSql('ALTER TABLE goles_date DROP player_id');
    }
}
