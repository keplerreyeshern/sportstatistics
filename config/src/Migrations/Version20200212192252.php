<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200212192252 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE goles_times (id INT AUTO_INCREMENT NOT NULL, gol_id INT DEFAULT NULL, team_id INT DEFAULT NULL, time INT NOT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_EB75D8C057A12A44 (gol_id), INDEX IDX_EB75D8C0296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE goles_date (id INT AUTO_INCREMENT NOT NULL, gol_id INT DEFAULT NULL, team_id INT DEFAULT NULL, minute DOUBLE PRECISION NOT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_B18E20AF57A12A44 (gol_id), INDEX IDX_B18E20AF296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE goles_times ADD CONSTRAINT FK_EB75D8C057A12A44 FOREIGN KEY (gol_id) REFERENCES goles (id)');
        $this->addSql('ALTER TABLE goles_times ADD CONSTRAINT FK_EB75D8C0296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE goles_date ADD CONSTRAINT FK_B18E20AF57A12A44 FOREIGN KEY (gol_id) REFERENCES goles (id)');
        $this->addSql('ALTER TABLE goles_date ADD CONSTRAINT FK_B18E20AF296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE goles_times');
        $this->addSql('DROP TABLE goles_date');
    }
}
