<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200220175651 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE assists (id INT AUTO_INCREMENT NOT NULL, player_id INT DEFAULT NULL, team_id INT DEFAULT NULL, game_id INT DEFAULT NULL, league_id INT DEFAULT NULL, INDEX IDX_11AF8FAB99E6F5DF (player_id), INDEX IDX_11AF8FAB296CD8AE (team_id), INDEX IDX_11AF8FABE48FD905 (game_id), INDEX IDX_11AF8FAB58AFC4DE (league_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE assists ADD CONSTRAINT FK_11AF8FAB99E6F5DF FOREIGN KEY (player_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE assists ADD CONSTRAINT FK_11AF8FAB296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE assists ADD CONSTRAINT FK_11AF8FABE48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id)');
        $this->addSql('ALTER TABLE assists ADD CONSTRAINT FK_11AF8FAB58AFC4DE FOREIGN KEY (league_id) REFERENCES leagues (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE assists');
    }
}
