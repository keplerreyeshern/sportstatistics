<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200220170948 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE players_leagues (players_id INT NOT NULL, leagues_id INT NOT NULL, INDEX IDX_1E0632B4F1849495 (players_id), INDEX IDX_1E0632B48642ED32 (leagues_id), PRIMARY KEY(players_id, leagues_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE players_leagues ADD CONSTRAINT FK_1E0632B4F1849495 FOREIGN KEY (players_id) REFERENCES players (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE players_leagues ADD CONSTRAINT FK_1E0632B48642ED32 FOREIGN KEY (leagues_id) REFERENCES leagues (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE players DROP FOREIGN KEY FK_264E43A658AFC4DE');
        $this->addSql('DROP INDEX IDX_264E43A658AFC4DE ON players');
        $this->addSql('ALTER TABLE players DROP league_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE players_leagues');
        $this->addSql('ALTER TABLE players ADD league_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE players ADD CONSTRAINT FK_264E43A658AFC4DE FOREIGN KEY (league_id) REFERENCES leagues (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_264E43A658AFC4DE ON players (league_id)');
    }
}
