<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200224191509 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE faults (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, committed_player_id INT DEFAULT NULL, received_player_id INT DEFAULT NULL, committed_team_id INT DEFAULT NULL, received_team_id INT DEFAULT NULL, minute INT NOT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_544EED87E48FD905 (game_id), INDEX IDX_544EED8788F838D6 (committed_player_id), INDEX IDX_544EED87124A583F (received_player_id), INDEX IDX_544EED8735BA6D7D (committed_team_id), INDEX IDX_544EED878B8FD2F7 (received_team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE penalties (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, player_id INT DEFAULT NULL, team_id INT DEFAULT NULL, minute INT NOT NULL, annotated TINYINT(1) NOT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_B89F70AFE48FD905 (game_id), INDEX IDX_B89F70AF99E6F5DF (player_id), INDEX IDX_B89F70AF296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE free_shots (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, player_id INT DEFAULT NULL, team_id INT DEFAULT NULL, minute INT NOT NULL, annotated TINYINT(1) NOT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_DC4FD537E48FD905 (game_id), INDEX IDX_DC4FD53799E6F5DF (player_id), INDEX IDX_DC4FD537296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shooting (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, player_id INT DEFAULT NULL, team_id INT DEFAULT NULL, minute INT NOT NULL, goal TINYINT(1) NOT NULL, annotated TINYINT(1) NOT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updeted_at DATETIME NOT NULL, INDEX IDX_9697633DE48FD905 (game_id), INDEX IDX_9697633D99E6F5DF (player_id), INDEX IDX_9697633D296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE corner_kicks (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, player_id INT DEFAULT NULL, team_id INT DEFAULT NULL, minute INT NOT NULL, annotated TINYINT(1) NOT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_3254D943E48FD905 (game_id), INDEX IDX_3254D94399E6F5DF (player_id), INDEX IDX_3254D943296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE faults ADD CONSTRAINT FK_544EED87E48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id)');
        $this->addSql('ALTER TABLE faults ADD CONSTRAINT FK_544EED8788F838D6 FOREIGN KEY (committed_player_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE faults ADD CONSTRAINT FK_544EED87124A583F FOREIGN KEY (received_player_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE faults ADD CONSTRAINT FK_544EED8735BA6D7D FOREIGN KEY (committed_team_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE faults ADD CONSTRAINT FK_544EED878B8FD2F7 FOREIGN KEY (received_team_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE penalties ADD CONSTRAINT FK_B89F70AFE48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id)');
        $this->addSql('ALTER TABLE penalties ADD CONSTRAINT FK_B89F70AF99E6F5DF FOREIGN KEY (player_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE penalties ADD CONSTRAINT FK_B89F70AF296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE free_shots ADD CONSTRAINT FK_DC4FD537E48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id)');
        $this->addSql('ALTER TABLE free_shots ADD CONSTRAINT FK_DC4FD53799E6F5DF FOREIGN KEY (player_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE free_shots ADD CONSTRAINT FK_DC4FD537296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE shooting ADD CONSTRAINT FK_9697633DE48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id)');
        $this->addSql('ALTER TABLE shooting ADD CONSTRAINT FK_9697633D99E6F5DF FOREIGN KEY (player_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE shooting ADD CONSTRAINT FK_9697633D296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE corner_kicks ADD CONSTRAINT FK_3254D943E48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id)');
        $this->addSql('ALTER TABLE corner_kicks ADD CONSTRAINT FK_3254D94399E6F5DF FOREIGN KEY (player_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE corner_kicks ADD CONSTRAINT FK_3254D943296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id)');
        $this->addSql('DROP TABLE positions_table');
        $this->addSql('DROP TABLE winner');
        $this->addSql('ALTER TABLE assists ADD minute INT NOT NULL');
        $this->addSql('ALTER TABLE lineups ADD alternates_local1_id INT DEFAULT NULL, ADD alternates_local2_id INT DEFAULT NULL, ADD alternates_local3_id INT DEFAULT NULL, ADD alternates_local4_id INT DEFAULT NULL, ADD alternates_local5_id INT DEFAULT NULL, ADD alternates_local6_id INT DEFAULT NULL, ADD alternates_visitor1_id INT DEFAULT NULL, ADD alternates_visitor2_id INT DEFAULT NULL, ADD alternates_visitor3_id INT DEFAULT NULL, ADD alternates_visitor4_id INT DEFAULT NULL, ADD alternates_visitor5_id INT DEFAULT NULL, ADD alternates_visitor6_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABCF7D3045 FOREIGN KEY (alternates_local1_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABDDC89FAB FOREIGN KEY (alternates_local2_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB6574F8CE FOREIGN KEY (alternates_local3_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABF8A3C077 FOREIGN KEY (alternates_local4_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB401FA712 FOREIGN KEY (alternates_local5_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB52AA08FC FOREIGN KEY (alternates_local6_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB57EFEBAA FOREIGN KEY (alternates_visitor1_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB455A4444 FOREIGN KEY (alternates_visitor2_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABFDE62321 FOREIGN KEY (alternates_visitor3_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AAB60311B98 FOREIGN KEY (alternates_visitor4_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABD88D7CFD FOREIGN KEY (alternates_visitor5_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE lineups ADD CONSTRAINT FK_60729AABCA38D313 FOREIGN KEY (alternates_visitor6_id) REFERENCES players (id)');
        $this->addSql('CREATE INDEX IDX_60729AABCF7D3045 ON lineups (alternates_local1_id)');
        $this->addSql('CREATE INDEX IDX_60729AABDDC89FAB ON lineups (alternates_local2_id)');
        $this->addSql('CREATE INDEX IDX_60729AAB6574F8CE ON lineups (alternates_local3_id)');
        $this->addSql('CREATE INDEX IDX_60729AABF8A3C077 ON lineups (alternates_local4_id)');
        $this->addSql('CREATE INDEX IDX_60729AAB401FA712 ON lineups (alternates_local5_id)');
        $this->addSql('CREATE INDEX IDX_60729AAB52AA08FC ON lineups (alternates_local6_id)');
        $this->addSql('CREATE INDEX IDX_60729AAB57EFEBAA ON lineups (alternates_visitor1_id)');
        $this->addSql('CREATE INDEX IDX_60729AAB455A4444 ON lineups (alternates_visitor2_id)');
        $this->addSql('CREATE INDEX IDX_60729AABFDE62321 ON lineups (alternates_visitor3_id)');
        $this->addSql('CREATE INDEX IDX_60729AAB60311B98 ON lineups (alternates_visitor4_id)');
        $this->addSql('CREATE INDEX IDX_60729AABD88D7CFD ON lineups (alternates_visitor5_id)');
        $this->addSql('CREATE INDEX IDX_60729AABCA38D313 ON lineups (alternates_visitor6_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE positions_table (id INT AUTO_INCREMENT NOT NULL, team_id INT DEFAULT NULL, league_id INT DEFAULT NULL, j INT NOT NULL, g INT NOT NULL, e INT NOT NULL, p INT NOT NULL, diff INT NOT NULL, pts INT NOT NULL, year INT NOT NULL, is_active TINYINT(1) NOT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_8CE25EBF296CD8AE (team_id), INDEX IDX_8CE25EBF58AFC4DE (league_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE winner (id INT AUTO_INCREMENT NOT NULL, ten_id INT DEFAULT NULL, thirty_id INT DEFAULT NULL, first_time_id INT DEFAULT NULL, end_id INT DEFAULT NULL, game_id INT DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_CF6600E325A8A5B (ten_id), INDEX IDX_CF6600E4D66B1C7 (first_time_id), INDEX IDX_CF6600E93E92DBE (thirty_id), INDEX IDX_CF6600EE2BD8A10 (end_id), UNIQUE INDEX UNIQ_CF6600EE48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE positions_table ADD CONSTRAINT FK_8CE25EBF296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE positions_table ADD CONSTRAINT FK_8CE25EBF58AFC4DE FOREIGN KEY (league_id) REFERENCES leagues (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE winner ADD CONSTRAINT FK_CF6600E325A8A5B FOREIGN KEY (ten_id) REFERENCES teams (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE winner ADD CONSTRAINT FK_CF6600E4D66B1C7 FOREIGN KEY (first_time_id) REFERENCES teams (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE winner ADD CONSTRAINT FK_CF6600E93E92DBE FOREIGN KEY (thirty_id) REFERENCES teams (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE winner ADD CONSTRAINT FK_CF6600EE2BD8A10 FOREIGN KEY (end_id) REFERENCES teams (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE winner ADD CONSTRAINT FK_CF6600EE48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('DROP TABLE faults');
        $this->addSql('DROP TABLE penalties');
        $this->addSql('DROP TABLE free_shots');
        $this->addSql('DROP TABLE shooting');
        $this->addSql('DROP TABLE corner_kicks');
        $this->addSql('ALTER TABLE assists DROP minute');
        $this->addSql('ALTER TABLE lineups DROP FOREIGN KEY FK_60729AABCF7D3045');
        $this->addSql('ALTER TABLE lineups DROP FOREIGN KEY FK_60729AABDDC89FAB');
        $this->addSql('ALTER TABLE lineups DROP FOREIGN KEY FK_60729AAB6574F8CE');
        $this->addSql('ALTER TABLE lineups DROP FOREIGN KEY FK_60729AABF8A3C077');
        $this->addSql('ALTER TABLE lineups DROP FOREIGN KEY FK_60729AAB401FA712');
        $this->addSql('ALTER TABLE lineups DROP FOREIGN KEY FK_60729AAB52AA08FC');
        $this->addSql('ALTER TABLE lineups DROP FOREIGN KEY FK_60729AAB57EFEBAA');
        $this->addSql('ALTER TABLE lineups DROP FOREIGN KEY FK_60729AAB455A4444');
        $this->addSql('ALTER TABLE lineups DROP FOREIGN KEY FK_60729AABFDE62321');
        $this->addSql('ALTER TABLE lineups DROP FOREIGN KEY FK_60729AAB60311B98');
        $this->addSql('ALTER TABLE lineups DROP FOREIGN KEY FK_60729AABD88D7CFD');
        $this->addSql('ALTER TABLE lineups DROP FOREIGN KEY FK_60729AABCA38D313');
        $this->addSql('DROP INDEX IDX_60729AABCF7D3045 ON lineups');
        $this->addSql('DROP INDEX IDX_60729AABDDC89FAB ON lineups');
        $this->addSql('DROP INDEX IDX_60729AAB6574F8CE ON lineups');
        $this->addSql('DROP INDEX IDX_60729AABF8A3C077 ON lineups');
        $this->addSql('DROP INDEX IDX_60729AAB401FA712 ON lineups');
        $this->addSql('DROP INDEX IDX_60729AAB52AA08FC ON lineups');
        $this->addSql('DROP INDEX IDX_60729AAB57EFEBAA ON lineups');
        $this->addSql('DROP INDEX IDX_60729AAB455A4444 ON lineups');
        $this->addSql('DROP INDEX IDX_60729AABFDE62321 ON lineups');
        $this->addSql('DROP INDEX IDX_60729AAB60311B98 ON lineups');
        $this->addSql('DROP INDEX IDX_60729AABD88D7CFD ON lineups');
        $this->addSql('DROP INDEX IDX_60729AABCA38D313 ON lineups');
        $this->addSql('ALTER TABLE lineups DROP alternates_local1_id, DROP alternates_local2_id, DROP alternates_local3_id, DROP alternates_local4_id, DROP alternates_local5_id, DROP alternates_local6_id, DROP alternates_visitor1_id, DROP alternates_visitor2_id, DROP alternates_visitor3_id, DROP alternates_visitor4_id, DROP alternates_visitor5_id, DROP alternates_visitor6_id');
    }
}
