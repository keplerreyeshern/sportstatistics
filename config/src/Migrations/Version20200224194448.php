<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200224194448 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE penalties ADD goalkeeper_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE penalties ADD CONSTRAINT FK_B89F70AFE1C697D6 FOREIGN KEY (goalkeeper_id) REFERENCES players (id)');
        $this->addSql('CREATE INDEX IDX_B89F70AFE1C697D6 ON penalties (goalkeeper_id)');
        $this->addSql('ALTER TABLE free_shots ADD goalkeeper_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE free_shots ADD CONSTRAINT FK_DC4FD537E1C697D6 FOREIGN KEY (goalkeeper_id) REFERENCES players (id)');
        $this->addSql('CREATE INDEX IDX_DC4FD537E1C697D6 ON free_shots (goalkeeper_id)');
        $this->addSql('ALTER TABLE shooting ADD goalkeeper_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shooting ADD CONSTRAINT FK_9697633DE1C697D6 FOREIGN KEY (goalkeeper_id) REFERENCES players (id)');
        $this->addSql('CREATE INDEX IDX_9697633DE1C697D6 ON shooting (goalkeeper_id)');
        $this->addSql('ALTER TABLE corner_kicks ADD goalkeeper_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE corner_kicks ADD CONSTRAINT FK_3254D943E1C697D6 FOREIGN KEY (goalkeeper_id) REFERENCES players (id)');
        $this->addSql('CREATE INDEX IDX_3254D943E1C697D6 ON corner_kicks (goalkeeper_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE corner_kicks DROP FOREIGN KEY FK_3254D943E1C697D6');
        $this->addSql('DROP INDEX IDX_3254D943E1C697D6 ON corner_kicks');
        $this->addSql('ALTER TABLE corner_kicks DROP goalkeeper_id');
        $this->addSql('ALTER TABLE free_shots DROP FOREIGN KEY FK_DC4FD537E1C697D6');
        $this->addSql('DROP INDEX IDX_DC4FD537E1C697D6 ON free_shots');
        $this->addSql('ALTER TABLE free_shots DROP goalkeeper_id');
        $this->addSql('ALTER TABLE penalties DROP FOREIGN KEY FK_B89F70AFE1C697D6');
        $this->addSql('DROP INDEX IDX_B89F70AFE1C697D6 ON penalties');
        $this->addSql('ALTER TABLE penalties DROP goalkeeper_id');
        $this->addSql('ALTER TABLE shooting DROP FOREIGN KEY FK_9697633DE1C697D6');
        $this->addSql('DROP INDEX IDX_9697633DE1C697D6 ON shooting');
        $this->addSql('ALTER TABLE shooting DROP goalkeeper_id');
    }
}
