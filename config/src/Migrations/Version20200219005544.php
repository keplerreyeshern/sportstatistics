<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200219005544 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cards DROP FOREIGN KEY FK_4C258FDE48FD905');
        $this->addSql('ALTER TABLE cards ADD CONSTRAINT FK_4C258FDE48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cards DROP FOREIGN KEY FK_4C258FDE48FD905');
        $this->addSql('ALTER TABLE cards ADD CONSTRAINT FK_4C258FDE48FD905 FOREIGN KEY (game_id) REFERENCES games_results (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
    }
}
