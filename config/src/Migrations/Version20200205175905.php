<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200205175905 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE winner (id INT AUTO_INCREMENT NOT NULL, ten_id INT DEFAULT NULL, thirty_id INT DEFAULT NULL, first_time_id INT DEFAULT NULL, end_id INT DEFAULT NULL, game_id INT DEFAULT NULL, INDEX IDX_CF6600E325A8A5B (ten_id), INDEX IDX_CF6600E93E92DBE (thirty_id), INDEX IDX_CF6600E4D66B1C7 (first_time_id), INDEX IDX_CF6600EE2BD8A10 (end_id), UNIQUE INDEX UNIQ_CF6600EE48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE winner ADD CONSTRAINT FK_CF6600E325A8A5B FOREIGN KEY (ten_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE winner ADD CONSTRAINT FK_CF6600E93E92DBE FOREIGN KEY (thirty_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE winner ADD CONSTRAINT FK_CF6600E4D66B1C7 FOREIGN KEY (first_time_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE winner ADD CONSTRAINT FK_CF6600EE2BD8A10 FOREIGN KEY (end_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE winner ADD CONSTRAINT FK_CF6600EE48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE winner');
    }
}
