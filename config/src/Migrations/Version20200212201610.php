<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200212201610 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE goles_date DROP FOREIGN KEY FK_B18E20AF57A12A44');
        $this->addSql('ALTER TABLE goles_times DROP FOREIGN KEY FK_EB75D8C057A12A44');
        $this->addSql('DROP TABLE goles');
        $this->addSql('DROP INDEX IDX_EB75D8C057A12A44 ON goles_times');
        $this->addSql('ALTER TABLE goles_times CHANGE gol_id game_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE goles_times ADD CONSTRAINT FK_EB75D8C0E48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id)');
        $this->addSql('CREATE INDEX IDX_EB75D8C0E48FD905 ON goles_times (game_id)');
        $this->addSql('DROP INDEX IDX_B18E20AF57A12A44 ON goles_date');
        $this->addSql('ALTER TABLE goles_date CHANGE gol_id game_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE goles_date ADD CONSTRAINT FK_B18E20AFE48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id)');
        $this->addSql('CREATE INDEX IDX_B18E20AFE48FD905 ON goles_date (game_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE goles (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_1E3E3635E48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE goles ADD CONSTRAINT FK_1E3E3635E48FD905 FOREIGN KEY (game_id) REFERENCES calendars (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE goles_date DROP FOREIGN KEY FK_B18E20AFE48FD905');
        $this->addSql('DROP INDEX IDX_B18E20AFE48FD905 ON goles_date');
        $this->addSql('ALTER TABLE goles_date CHANGE game_id gol_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE goles_date ADD CONSTRAINT FK_B18E20AF57A12A44 FOREIGN KEY (gol_id) REFERENCES goles (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_B18E20AF57A12A44 ON goles_date (gol_id)');
        $this->addSql('ALTER TABLE goles_times DROP FOREIGN KEY FK_EB75D8C0E48FD905');
        $this->addSql('DROP INDEX IDX_EB75D8C0E48FD905 ON goles_times');
        $this->addSql('ALTER TABLE goles_times CHANGE game_id gol_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE goles_times ADD CONSTRAINT FK_EB75D8C057A12A44 FOREIGN KEY (gol_id) REFERENCES goles (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_EB75D8C057A12A44 ON goles_times (gol_id)');
    }
}
