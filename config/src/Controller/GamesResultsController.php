<?php

namespace App\Controller;

use App\Entity\Calendars;
use App\Entity\Cards;
use App\Entity\CornerKicks;
use App\Entity\GamesResults;
use App\Entity\Injuries;
use App\Entity\Leagues;
use App\Entity\Offsides;
use App\Entity\VariablesLeagues;
use App\Form\GamesResultsType;
use App\Repository\CalendarsRepository;
use App\Repository\GamesResultsRepository;
use App\Repository\GolesDateRepository;
use App\Repository\StatisticsGameRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/games/results")
 */
class GamesResultsController extends AbstractController
{
    /**
     * @Route("/", name="app_games_result", methods={"GET"})
     */
    public function index(GamesResultsRepository $gamesResultsRepository, GolesDateRepository $golesDateRepository, StatisticsGameRepository $statisticsGameRepository): Response
    {
        $games_results = $gamesResultsRepository->findAll();
        $leagues = $this->getDoctrine()->getRepository(Leagues::class)->findAll();
        $active  = 'games_results';
        $journeyActive = $this->getDoctrine()->getRepository(VariablesLeagues::class)->findOneBy(['isActive' => true]);
        $journeyActive = $journeyActive->getJourney();
        $journeys = $this->getDoctrine()->getRepository(Calendars::class)->journeys(1);
        $calendars = $this->getDoctrine()->getRepository(Calendars::class)->findBy(['journey' => $journeyActive -1, 'year' => date('Y')]);
        return $this->render('games_results/index.html.twig', compact('games_results', 'leagues', 'active', 'journeys', 'journeyActive', 'gamesResultsRepository', 'calendars', 'golesDateRepository', 'statisticsGameRepository'));
    }

    /**
     * @Route("/position/table", name="position_table", methods={"GET"})
     */
    public function positiontable(Request $request, GamesResultsRepository $gamesResultsRepository)
    {
        $tablePosition = $gamesResultsRepository->gamesplayed(1, 1);
        $active = 'position_table';
        $injuries = $this->getDoctrine()->getRepository(Injuries::class)->findAll();
        $cards = $this->getDoctrine()->getRepository(Cards::class)->findAll();
        $scoreboard = $this->getDoctrine()->getRepository(GamesResults::class)->scoreboard(1, 1);
        $cornerKicks = $this->getDoctrine()->getRepository(GamesResults::class)->cornersKicks();
        $offsides = $this->getDoctrine()->getRepository(GamesResults::class)->offSides();
        return $this->render('games_results/positiontable.html.twig', compact('tablePosition', 'active', 'injuries', 'cards', 'scoreboard', 'cornerKicks', 'offsides'));
    }

    /**
     * @Route("/rolan/{journey}", name="app_games_result_rolan", methods={"GET"})
     */
    public function rolan(Request $request, GamesResultsRepository $gamesResultsRepository, CalendarsRepository $calendarsRepository, GolesDateRepository $golesDateRepository, StatisticsGameRepository $statisticsGameRepository)
    {
        $journey = $request->get('journey');
        $calendars = $calendarsRepository->findBy(['journey' => $journey, 'year' => date('Y')]);
        return $this->render('games_results/rolan.html.twig', compact('calendars', 'gamesResultsRepository', 'golesDateRepository', 'statisticsGameRepository'));
    }

    /**
     * @Route("/new", name="games_results_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $gamesResult = new GamesResults();
        $form = $this->createForm(GamesResultsType::class, $gamesResult);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($gamesResult);
            $entityManager->flush();

            return $this->redirectToRoute('games_results_index');
        }

        return $this->render('games_results/new.html.twig', [
            'games_result' => $gamesResult,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="games_results_show", methods={"GET"})
     */
    public function show(GamesResults $gamesResult): Response
    {
        return $this->render('games_results/show.html.twig', [
            'games_result' => $gamesResult,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="games_results_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, GamesResults $gamesResult): Response
    {
        $form = $this->createForm(GamesResultsType::class, $gamesResult);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('games_results_index');
        }

        return $this->render('games_results/edit.html.twig', [
            'games_result' => $gamesResult,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="games_results_delete", methods={"DELETE"})
     */
    public function delete(Request $request, GamesResults $gamesResult): Response
    {
        if ($this->isCsrfTokenValid('delete'.$gamesResult->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($gamesResult);
            $entityManager->flush();
        }

        return $this->redirectToRoute('games_results_index');
    }
}
