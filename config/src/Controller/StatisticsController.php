<?php

namespace App\Controller;

use App\Entity\Calendars;
use App\Entity\GamesResults;
use App\Entity\Leagues;
use App\Entity\Teams;
use App\Entity\VariablesLeagues;
use App\Repository\GamesResultsRepository;
use App\Repository\GolesDateRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * @Route("/statistics")
 */
class StatisticsController extends AbstractController
{
    /**
     * @Route("/", name="statistics_index", methods={"GET"})
     */
    public function index()
    {
        $active = 'statistics';
        $league = $this->getDoctrine()->getRepository(Leagues::class)->findOneBy(['id' => 1]);
        $leagueName = $league->getLeague();
        $leagueId = $league->getId();
        $teams = $this->getDoctrine()->getRepository(Teams::class)->findBy(['league' => $leagueId]);
        return $this->render('statistics/index.html.twig', compact('leagueName', 'active', 'teams'));
    }

    /**
     * @Route("/{id}", name="statistics_show", methods={"GET"})
     */
    public function show(Teams $team): Response
    {
        $active = 'statistics';
        $league = $this->getDoctrine()->getRepository(Leagues::class)->findOneBy(['id' => 1]);
        $leagueName = $league->getLeague();
        $leagueId = $league->getId();
        $journey = $this->getDoctrine()->getRepository(VariablesLeagues::class)->findOneBy(['isActive' => true]);
        $statistics = $this->getDoctrine()->getRepository(GamesResults::class)->statisticsForTeam($team->getId(), $journey->getJourney() - 1, 1);
        return $this->render('statistics/show.html.twig', compact('team', 'active', 'leagueName', 'statistics'));
    }
    /**
     * @Route("/for/game/{id}", name="statistics_show_game", methods={"GET"})
     */
    public function showForGame(Calendars $calendars, GolesDateRepository $golesDateRepository): Response
    {
        $active = 'statistics';
        $league = $this->getDoctrine()->getRepository(Leagues::class)->findOneBy(['id' => 1]);
        $leagueName = $league->getLeague();
        $leagueId = $league->getId();
        $journey = $this->getDoctrine()->getRepository(VariablesLeagues::class)->findOneBy(['isActive' => true]);
        $statistics = $this->getDoctrine()->getRepository(GamesResults::class)->staticstisForGame($calendars->getId());
        return $this->render('statistics/show_for_game.html.twig', compact('active', 'golesDateRepository', 'statistics'));
    }
    /**
     * @Route("/next/game/{id}", name="statistics_show_next_game", methods={"GET"})
     */
    public function showNextGame(Calendars $calendars, GolesDateRepository $golesDateRepository, GamesResultsRepository $gamesResultsRepository): Response
    {
        $active = 'statistics';
        $league = $this->getDoctrine()->getRepository(Leagues::class)->findOneBy(['id' => 1]);
        $leagueName = $league->getLeague();
        $leagueId = $league->getId();
        $journey = $this->getDoctrine()->getRepository(VariablesLeagues::class)->findOneBy(['isActive' => true]);
        $journey = $journey->getJourney();
        $statistics = $this->getDoctrine()->getRepository(GamesResults::class)->backGames($calendars->getTeamLocal()->getId(), $calendars->getTeamVisitor()->getId());
        return $this->render('statistics/show_next_game.html.twig', compact('calendars','active', 'golesDateRepository', 'statistics', 'gamesResultsRepository', 'journey'));
    }

    /**
     * @Route("/gather/information", name="statistics_gather_information", methods={"GET"})
     */
    public function gatherInformation(): Response
    {
        $active = 'statistics';
        $text = file_get_contents("https://www.marca.com/claro-mx/futbol/liga-mx/apertura/calendario.html", FILE_USE_INCLUDE_PATH);

        $journeysleague_m_x = $this->getDoctrine()->getRepository(Calendars::class)->findBy(['year' => 2019]);
        $allJorney = 0;
        $journey = 0;
        foreach ($journeysleague_m_x as $row){
            $allJorney++;
        }
        if ($allJorney == 0){
            $journey = 1;
        } elseif ($allJorney <= 9){
            $journey = 2;
        } elseif ($allJorney <= 18){
            $journey = 3;
        } elseif ($allJorney <= 27){
            $journey = 4;
        } elseif ($allJorney <= 36){
            $journey = 5;
        } elseif ($allJorney <= 45){
            $journey = 6;
        } elseif ($allJorney <= 54){
            $journey = 7;
        } elseif ($allJorney <= 63){
            $journey = 8;
        } elseif ($allJorney <= 72){
            $journey = 9;
        } elseif ($allJorney <= 81){
            $journey = 10;
        } elseif ($allJorney <= 90){
            $journey = 11;
        } elseif ($allJorney <= 99){
            $journey = 12;
        } elseif ($allJorney <= 118){
            $journey = 13;
        } elseif ($allJorney <= 127){
            $journey = 14;
        } elseif ($allJorney <= 136){
            $journey = 15;
        } elseif ($allJorney <= 145){
            $journey = 16;
        } elseif ($allJorney <= 154){
            $journey = 17;
        } elseif ($allJorney <= 163){
            $journey = 18;
        } elseif ($allJorney <= 172){
            $journey = 19;
        }

        $pos = strpos($text, 'id="jornada'.$journey.'"');
        $pos = $pos - 59;
        $length = 7971;
        $table = substr($text, $pos, $length);
        return $this->render('statistics/gather_information.html.twig', compact('active', 'table', 'journey'));
    }

    /**
     * @Route("/insert", name="statistics_m_x_insert", methods={"GET","POST"})

     */
    public function insert()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $rows = json_decode($_POST['array'], true);
        foreach ($rows as $row){
            $calendar = new Calendars();
            $gameResult = new GamesResults();
            $teamLocal = $this->getDoctrine()->getRepository(Teams::class)->findOneBy(['name' => $row['teamLocal']]);
            $teamVisitor = $this->getDoctrine()->getRepository(Teams::class)->findOneBy(['name' => $row['teamVisitor']]);
            $calendar->setJourney((int)$row['number']);
            $calendar->setTeamLocal($teamLocal);
            $calendar->setTeamVisitor($teamVisitor);
            $calendar->setSlug('falta');
            $calendar->setYear(2019);
            $calendar->setDate(date_create('now'));
            $entityManager->persist($calendar);
//            echo $teamsRepository->findOneBy(['team' => $row['teamLocal']])->getId().'<br>';
            $entityManager->flush();
            $gameResult->setGame($calendar);
            $gameResult->setMarkerLocal((int)$row['markerLocal']);
            $gameResult->setMarkerVisitor((int)$row['markerVisitor']);
            $entityManager->persist($gameResult);
            $entityManager->flush();
        }
        var_dump($rows);
//        return $this->redirectToRoute('league_m_x_index');
    }


}
