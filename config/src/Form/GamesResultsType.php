<?php

namespace App\Form;

use App\Entity\GamesResults;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GamesResultsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('markerLocal')
            ->add('markerVisitor')
            ->add('deletedAt')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('game')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GamesResults::class,
        ]);
    }
}
