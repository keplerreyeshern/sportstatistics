<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FreeShotsRepository")
 */
class FreeShots
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Calendars", inversedBy="freeShots")
     */
    private $game;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="freeShots")
     */
    private $player;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teams", inversedBy="freeShots")
     */
    private $team;

    /**
     * @ORM\Column(type="integer")
     */
    private $minute;

    /**
     * @ORM\Column(type="boolean")
     */
    private $annotated;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players")
     */
    private $goalkeeper;

    /**
     * @ORM\Column(type="boolean")
     */
    private $goal;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame(): ?Calendars
    {
        return $this->game;
    }

    public function setGame(?Calendars $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getPlayer(): ?Players
    {
        return $this->player;
    }

    public function setPlayer(?Players $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getTeam(): ?Teams
    {
        return $this->team;
    }

    public function setTeam(?Teams $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getMinute(): ?int
    {
        return $this->minute;
    }

    public function setMinute(int $minute): self
    {
        $this->minute = $minute;

        return $this;
    }

    public function getAnnotated(): ?bool
    {
        return $this->annotated;
    }

    public function setAnnotated(bool $annotated): self
    {
        $this->annotated = $annotated;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getGoalkeeper(): ?Players
    {
        return $this->goalkeeper;
    }

    public function setGoalkeeper(?Players $goalkeeper): self
    {
        $this->goalkeeper = $goalkeeper;

        return $this;
    }

    public function getGoal(): ?bool
    {
        return $this->goal;
    }

    public function setGoal(bool $goal): self
    {
        $this->goal = $goal;

        return $this;
    }
}
