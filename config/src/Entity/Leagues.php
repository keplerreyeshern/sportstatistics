<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LeaguesRepository")
 */
class Leagues
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $league;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Calendars", mappedBy="league")
     */
    private $calendars;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VariablesLeagues", mappedBy="league")
     */
    private $variablesLeagues;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Teams", mappedBy="league")
     */
    private $teams;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tournament", mappedBy="league")
     */
    private $tournaments;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GolesDate", mappedBy="league")
     */
    private $golesDates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Assists", mappedBy="league")
     */
    private $assists;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Players", mappedBy="league")
     */
    private $players;


    public function __construct()
    {
        $this->calendars = new ArrayCollection();
        $this->variablesLeagues = new ArrayCollection();
        $this->teams = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->isActive = true;
        $this->tournaments = new ArrayCollection();
        $this->golesDates = new ArrayCollection();
        $this->players = new ArrayCollection();
        $this->assists = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLeague(): ?string
    {
        return $this->league;
    }

    public function setLeague(string $league): self
    {
        $this->league = $league;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|Calendars[]
     */
    public function getCalendars(): Collection
    {
        return $this->calendars;
    }

    public function addCalendar(Calendars $calendar): self
    {
        if (!$this->calendars->contains($calendar)) {
            $this->calendars[] = $calendar;
            $calendar->setLeague($this);
        }

        return $this;
    }

    public function removeCalendar(Calendars $calendar): self
    {
        if ($this->calendars->contains($calendar)) {
            $this->calendars->removeElement($calendar);
            // set the owning side to null (unless already changed)
            if ($calendar->getLeague() === $this) {
                $calendar->setLeague(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|VariablesLeagues[]
     */
    public function getVariablesLeagues(): Collection
    {
        return $this->variablesLeagues;
    }

    public function addVariablesLeague(VariablesLeagues $variablesLeague): self
    {
        if (!$this->variablesLeagues->contains($variablesLeague)) {
            $this->variablesLeagues[] = $variablesLeague;
            $variablesLeague->setLeague($this);
        }

        return $this;
    }

    public function removeVariablesLeague(VariablesLeagues $variablesLeague): self
    {
        if ($this->variablesLeagues->contains($variablesLeague)) {
            $this->variablesLeagues->removeElement($variablesLeague);
            // set the owning side to null (unless already changed)
            if ($variablesLeague->getLeague() === $this) {
                $variablesLeague->setLeague(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->league;
    }

    /**
     * @return Collection|Teams[]
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function addTeam(Teams $team): self
    {
        if (!$this->teams->contains($team)) {
            $this->teams[] = $team;
            $team->setLeague($this);
        }

        return $this;
    }

    public function removeTeam(Teams $team): self
    {
        if ($this->teams->contains($team)) {
            $this->teams->removeElement($team);
            // set the owning side to null (unless already changed)
            if ($team->getLeague() === $this) {
                $team->setLeague(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GamesResults[]
     */
    public function getGamesResults(): Collection
    {
        return $this->gamesResults;
    }

    public function addGamesResult(GamesResults $gamesResult): self
    {
        if (!$this->gamesResults->contains($gamesResult)) {
            $this->gamesResults[] = $gamesResult;
            $gamesResult->setLeague($this);
        }

        return $this;
    }

    public function removeGamesResult(GamesResults $gamesResult): self
    {
        if ($this->gamesResults->contains($gamesResult)) {
            $this->gamesResults->removeElement($gamesResult);
            // set the owning side to null (unless already changed)
            if ($gamesResult->getLeague() === $this) {
                $gamesResult->setLeague(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tournament[]
     */
    public function getTournaments(): Collection
    {
        return $this->tournaments;
    }

    public function addTournament(Tournament $tournament): self
    {
        if (!$this->tournaments->contains($tournament)) {
            $this->tournaments[] = $tournament;
            $tournament->setLeague($this);
        }

        return $this;
    }

    public function removeTournament(Tournament $tournament): self
    {
        if ($this->tournaments->contains($tournament)) {
            $this->tournaments->removeElement($tournament);
            // set the owning side to null (unless already changed)
            if ($tournament->getLeague() === $this) {
                $tournament->setLeague(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|GolesDate[]
     */
    public function getGolesDates(): Collection
    {
        return $this->golesDates;
    }

    public function addGolesDate(GolesDate $golesDate): self
    {
        if (!$this->golesDates->contains($golesDate)) {
            $this->golesDates[] = $golesDate;
            $golesDate->setLeague($this);
        }

        return $this;
    }

    public function removeGolesDate(GolesDate $golesDate): self
    {
        if ($this->golesDates->contains($golesDate)) {
            $this->golesDates->removeElement($golesDate);
            // set the owning side to null (unless already changed)
            if ($golesDate->getLeague() === $this) {
                $golesDate->setLeague(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Assists[]
     */
    public function getAssists(): Collection
    {
        return $this->assists;
    }

    public function addAssist(Assists $assist): self
    {
        if (!$this->assists->contains($assist)) {
            $this->assists[] = $assist;
            $assist->setLeague($this);
        }

        return $this;
    }

    public function removeAssist(Assists $assist): self
    {
        if ($this->assists->contains($assist)) {
            $this->assists->removeElement($assist);
            // set the owning side to null (unless already changed)
            if ($assist->getLeague() === $this) {
                $assist->setLeague(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Players[]
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    public function addPlayer(Players $player): self
    {
        if (!$this->players->contains($player)) {
            $this->players[] = $player;
            $player->setLeague($this);
        }

        return $this;
    }

    public function removePlayer(Players $player): self
    {
        if ($this->players->contains($player)) {
            $this->players->removeElement($player);
            // set the owning side to null (unless already changed)
            if ($player->getLeague() === $this) {
                $player->setLeague(null);
            }
        }

        return $this;
    }
}
