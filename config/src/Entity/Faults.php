<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FaultsRepository")
 */
class Faults
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Calendars", inversedBy="faults")
     */
    private $game;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="faults")
     */
    private $committedPlayer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Players", inversedBy="faults")
     */
    private $receivedPlayer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teams", inversedBy="faults")
     */
    private $committedTeam;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teams", inversedBy="faults")
     */
    private $receivedTeam;

    /**
     * @ORM\Column(type="integer")
     */
    private $minute;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame(): ?Calendars
    {
        return $this->game;
    }

    public function setGame(?Calendars $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getCommittedPlayer(): ?Players
    {
        return $this->committedPlayer;
    }

    public function setCommittedPlayer(?Players $committedPlayer): self
    {
        $this->committedPlayer = $committedPlayer;

        return $this;
    }

    public function getReceivedPlayer(): ?Players
    {
        return $this->receivedPlayer;
    }

    public function setReceivedPlayer(?Players $receivedPlayer): self
    {
        $this->receivedPlayer = $receivedPlayer;

        return $this;
    }

    public function getCommittedTeam(): ?Teams
    {
        return $this->committedTeam;
    }

    public function setCommittedTeam(?Teams $committedTeam): self
    {
        $this->committedTeam = $committedTeam;

        return $this;
    }

    public function getReceivedTeam(): ?Teams
    {
        return $this->receivedTeam;
    }

    public function setReceivedTeam(?Teams $receivedTeam): self
    {
        $this->receivedTeam = $receivedTeam;

        return $this;
    }

    public function getMinute(): ?int
    {
        return $this->minute;
    }

    public function setMinute(int $minute): self
    {
        $this->minute = $minute;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
