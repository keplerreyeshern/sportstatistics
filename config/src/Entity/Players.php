<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayersRepository")
 * @Vich\Uploadable
 */
class Players
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SoccerPositions", inversedBy="players")
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teams", inversedBy="players")
     */
    private $team;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teams", inversedBy="players")
     */
    private $teamItComesFrom;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GolesDate", mappedBy="player")
     */
    private $golesDates;

    /**
     * @ORM\Column(type="integer")
     */
    private $number;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateOfBirth;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $ContractUntil;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $marketValue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @Vich\UploadableField(mapping="faces_players", fileNameProperty="photo")
     * @var File
     */
    private $photoFile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Injuries", mappedBy="player")
     */
    private $injuries;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cards", mappedBy="player")
     */
    private $cards;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Nationalities", inversedBy="players")
     */
    private $nationality;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Assists", mappedBy="player")
     */
    private $assists;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Leagues", inversedBy="players")
     */
    private $league;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Lineups", mappedBy="defending1Local")
     */
    private $lineups;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FreeShots", mappedBy="player")
     */
    private $freeShots;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Penalties", mappedBy="player")
     */
    private $penalties;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Faults", mappedBy="committedPlayer")
     */
    private $faults;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CornerKicks", mappedBy="player")
     */
    private $cornerKicks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Shooting", mappedBy="player")
     */
    private $shootings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offsides", mappedBy="player")
     */
    private $offsides;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $percentage;


    public function __construct()
    {
        $this->golesDates = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->injuries = new ArrayCollection();
        $this->cards = new ArrayCollection();
        $this->leagues = new ArrayCollection();
        $this->assists = new ArrayCollection();
        $this->lineups = new ArrayCollection();
        $this->lineupss = new ArrayCollection();
        $this->freeShots = new ArrayCollection();
        $this->penalties = new ArrayCollection();
        $this->faults = new ArrayCollection();
        $this->cornerKicks = new ArrayCollection();
        $this->shootings = new ArrayCollection();
        $this->offsides = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPosition(): ?SoccerPositions
    {
        return $this->position;
    }

    public function setPosition(?SoccerPositions $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getTeam(): ?Teams
    {
        return $this->team;
    }

    public function setTeam(?Teams $team): self
    {
        $this->team = $team;

        return $this;
    }


    public function getTeamItComesFrom(): ?Teams
    {
        return $this->teamItComesFrom;
    }

    public function setTeamItComesFrom(?Teams $teamItComesFrom): self
    {
        $this->teamItComesFrom = $teamItComesFrom;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|GolesDate[]
     */
    public function getGolesDates(): Collection
    {
        return $this->golesDates;
    }

    public function addGolesDate(GolesDate $golesDate): self
    {
        if (!$this->golesDates->contains($golesDate)) {
            $this->golesDates[] = $golesDate;
            $golesDate->setPlayer($this);
        }

        return $this;
    }

    public function removeGolesDate(GolesDate $golesDate): self
    {
        if ($this->golesDates->contains($golesDate)) {
            $this->golesDates->removeElement($golesDate);
            // set the owning side to null (unless already changed)
            if ($golesDate->getPlayer() === $this) {
                $golesDate->setPlayer(null);
            }
        }

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getDateOfBirth(): ?\DateTimeInterface
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(?\DateTimeInterface $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }


    public function getContractUntil(): ?\DateTimeInterface
    {
        return $this->ContractUntil;
    }

    public function setContractUntil(?\DateTimeInterface $ContractUntil): self
    {
        $this->ContractUntil = $ContractUntil;

        return $this;
    }

    public function getMarketValue(): ?float
    {
        return $this->marketValue;
    }

    public function setMarketValue(?float $marketValue): self
    {
        $this->marketValue = $marketValue;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function setPhotoFile(File $photo = null)
    {
        $this->photoFile = $photo;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($photo) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getPhotoFile()
    {
        return $this->photoFile;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->name. ", Equipo ->". $this->team;
    }

    /**
     * @return Collection|Injuries[]
     */
    public function getInjuries(): Collection
    {
        return $this->injuries;
    }

    public function addInjury(Injuries $injury): self
    {
        if (!$this->injuries->contains($injury)) {
            $this->injuries[] = $injury;
            $injury->setPlayer($this);
        }

        return $this;
    }

    public function removeInjury(Injuries $injury): self
    {
        if ($this->injuries->contains($injury)) {
            $this->injuries->removeElement($injury);
            // set the owning side to null (unless already changed)
            if ($injury->getPlayer() === $this) {
                $injury->setPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cards[]
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    public function addCard(Cards $card): self
    {
        if (!$this->cards->contains($card)) {
            $this->cards[] = $card;
            $card->setPlayer($this);
        }

        return $this;
    }

    public function removeCard(Cards $card): self
    {
        if ($this->cards->contains($card)) {
            $this->cards->removeElement($card);
            // set the owning side to null (unless already changed)
            if ($card->getPlayer() === $this) {
                $card->setPlayer(null);
            }
        }

        return $this;
    }

    public function getNationality(): ?Nationalities
    {
        return $this->nationality;
    }

    public function setNationality(?Nationalities $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }


    /**
     * @return Collection|Assists[]
     */
    public function getAssists(): Collection
    {
        return $this->assists;
    }

    public function addAssist(Assists $assist): self
    {
        if (!$this->assists->contains($assist)) {
            $this->assists[] = $assist;
            $assist->setPlayer($this);
        }

        return $this;
    }

    public function removeAssist(Assists $assist): self
    {
        if ($this->assists->contains($assist)) {
            $this->assists->removeElement($assist);
            // set the owning side to null (unless already changed)
            if ($assist->getPlayer() === $this) {
                $assist->setPlayer(null);
            }
        }

        return $this;
    }

    public function getLeague(): ?Leagues
    {
        return $this->league;
    }

    public function setLeague(?Leagues $league): self
    {
        $this->league = $league;

        return $this;
    }

    /**
     * @return Collection|Lineups[]
     */
    public function getLineups(): Collection
    {
        return $this->lineups;
    }

    public function addLineup(Lineups $lineup): self
    {
        if (!$this->lineups->contains($lineup)) {
            $this->lineups[] = $lineup;
            $lineup->setDefending1Local($this);
        }

        return $this;
    }

    public function removeLineup(Lineups $lineup): self
    {
        if ($this->lineups->contains($lineup)) {
            $this->lineups->removeElement($lineup);
            // set the owning side to null (unless already changed)
            if ($lineup->getDefending1Local() === $this) {
                $lineup->setDefending1Local(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|FreeShots[]
     */
    public function getFreeShots(): Collection
    {
        return $this->freeShots;
    }

    public function addFreeShot(FreeShots $freeShot): self
    {
        if (!$this->freeShots->contains($freeShot)) {
            $this->freeShots[] = $freeShot;
            $freeShot->setPlayer($this);
        }

        return $this;
    }

    public function removeFreeShot(FreeShots $freeShot): self
    {
        if ($this->freeShots->contains($freeShot)) {
            $this->freeShots->removeElement($freeShot);
            // set the owning side to null (unless already changed)
            if ($freeShot->getPlayer() === $this) {
                $freeShot->setPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Penalties[]
     */
    public function getPenalties(): Collection
    {
        return $this->penalties;
    }

    public function addPenalty(Penalties $penalty): self
    {
        if (!$this->penalties->contains($penalty)) {
            $this->penalties[] = $penalty;
            $penalty->setPlayer($this);
        }

        return $this;
    }

    public function removePenalty(Penalties $penalty): self
    {
        if ($this->penalties->contains($penalty)) {
            $this->penalties->removeElement($penalty);
            // set the owning side to null (unless already changed)
            if ($penalty->getPlayer() === $this) {
                $penalty->setPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Faults[]
     */
    public function getFaults(): Collection
    {
        return $this->faults;
    }

    public function addFault(Faults $fault): self
    {
        if (!$this->faults->contains($fault)) {
            $this->faults[] = $fault;
            $fault->setCommittedPlayer($this);
        }

        return $this;
    }

    public function removeFault(Faults $fault): self
    {
        if ($this->faults->contains($fault)) {
            $this->faults->removeElement($fault);
            // set the owning side to null (unless already changed)
            if ($fault->getCommittedPlayer() === $this) {
                $fault->setCommittedPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CornerKicks[]
     */
    public function getCornerKicks(): Collection
    {
        return $this->cornerKicks;
    }

    public function addCornerKick(CornerKicks $cornerKick): self
    {
        if (!$this->cornerKicks->contains($cornerKick)) {
            $this->cornerKicks[] = $cornerKick;
            $cornerKick->setPlayer($this);
        }

        return $this;
    }

    public function removeCornerKick(CornerKicks $cornerKick): self
    {
        if ($this->cornerKicks->contains($cornerKick)) {
            $this->cornerKicks->removeElement($cornerKick);
            // set the owning side to null (unless already changed)
            if ($cornerKick->getPlayer() === $this) {
                $cornerKick->setPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Shooting[]
     */
    public function getShootings(): Collection
    {
        return $this->shootings;
    }

    public function addShooting(Shooting $shooting): self
    {
        if (!$this->shootings->contains($shooting)) {
            $this->shootings[] = $shooting;
            $shooting->setPlayer($this);
        }

        return $this;
    }

    public function removeShooting(Shooting $shooting): self
    {
        if ($this->shootings->contains($shooting)) {
            $this->shootings->removeElement($shooting);
            // set the owning side to null (unless already changed)
            if ($shooting->getPlayer() === $this) {
                $shooting->setPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Offsides[]
     */
    public function getOffsides(): Collection
    {
        return $this->offsides;
    }

    public function addOffside(Offsides $offside): self
    {
        if (!$this->offsides->contains($offside)) {
            $this->offsides[] = $offside;
            $offside->setPlayer($this);
        }

        return $this;
    }

    public function removeOffside(Offsides $offside): self
    {
        if ($this->offsides->contains($offside)) {
            $this->offsides->removeElement($offside);
            // set the owning side to null (unless already changed)
            if ($offside->getPlayer() === $this) {
                $offside->setPlayer(null);
            }
        }

        return $this;
    }

    public function getPercentage(): ?float
    {
        return $this->percentage;
    }

    public function setPercentage(?float $percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

}
