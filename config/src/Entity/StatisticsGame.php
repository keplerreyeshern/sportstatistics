<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StatisticsGameRepository")
 */
class StatisticsGame
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Calendars", inversedBy="statisticsGames")
     */
    private $game;

    /**
     * @ORM\Column(type="integer")
     */
    private $faultsLocal;

    /**
     * @ORM\Column(type="integer")
     */
    private $faultsVisitor;

    /**
     * @ORM\Column(type="integer")
     */
    private $redsLocal;

    /**
     * @ORM\Column(type="integer")
     */
    private $redsVisitor;

    /**
     * @ORM\Column(type="integer")
     */
    private $yellowsLocal;

    /**
     * @ORM\Column(type="integer")
     */
    private $yellowsVisitor;

    /**
     * @ORM\Column(type="integer")
     */
    private $offsideLocal;

    /**
     * @ORM\Column(type="integer")
     */
    private $offsideVisitor;

    /**
     * @ORM\Column(type="integer")
     */
    private $cornersLocal;

    /**
     * @ORM\Column(type="integer")
     */
    private $cornersVisitor;

    /**
     * @ORM\Column(type="integer")
     */
    private $savedLocal;

    /**
     * @ORM\Column(type="integer")
     */
    private $savedVisitor;

    /**
     * @ORM\Column(type="integer")
     */
    private $possessionLocal;

    /**
     * @ORM\Column(type="integer")
     */
    private $possessionVisitor;

    /**
     * @ORM\Column(type="integer")
     */
    private $goalShotsLocal;

    /**
     * @ORM\Column(type="integer")
     */
    private $goalShotsVisitor;

    /**
     * @ORM\Column(type="integer")
     */
    private $kicksToGoalLocal;

    /**
     * @ORM\Column(type="integer")
     */
    private $kicksToGoalVisitor;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame(): ?Calendars
    {
        return $this->game;
    }

    public function setGame(?Calendars $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getFaultsLocal(): ?int
    {
        return $this->faultsLocal;
    }

    public function setFaultsLocal(int $faultsLocal): self
    {
        $this->faultsLocal = $faultsLocal;

        return $this;
    }

    public function getFaultsVisitor(): ?int
    {
        return $this->faultsVisitor;
    }

    public function setFaultsVisitor(int $faultsVisitor): self
    {
        $this->faultsVisitor = $faultsVisitor;

        return $this;
    }

    public function getRedsLocal(): ?int
    {
        return $this->redsLocal;
    }

    public function setRedsLocal(int $redsLocal): self
    {
        $this->redsLocal = $redsLocal;

        return $this;
    }

    public function getRedsVisitor(): ?int
    {
        return $this->redsVisitor;
    }

    public function setRedsVisitor(int $redsVisitor): self
    {
        $this->redsVisitor = $redsVisitor;

        return $this;
    }

    public function getYellowsLocal(): ?int
    {
        return $this->yellowsLocal;
    }

    public function setYellowsLocal(int $yellowsLocal): self
    {
        $this->yellowsLocal = $yellowsLocal;

        return $this;
    }

    public function getYellowsVisitor(): ?int
    {
        return $this->yellowsVisitor;
    }

    public function setYellowsVisitor(int $yellowsVisitor): self
    {
        $this->yellowsVisitor = $yellowsVisitor;

        return $this;
    }

    public function getOffsideLocal(): ?int
    {
        return $this->offsideLocal;
    }

    public function setOffsideLocal(int $offsideLocal): self
    {
        $this->offsideLocal = $offsideLocal;

        return $this;
    }

    public function getOffsideVisitor(): ?int
    {
        return $this->offsideVisitor;
    }

    public function setOffsideVisitor(int $offsideVisitor): self
    {
        $this->offsideVisitor = $offsideVisitor;

        return $this;
    }

    public function getCornersLocal(): ?int
    {
        return $this->cornersLocal;
    }

    public function setCornersLocal(int $cornersLocal): self
    {
        $this->cornersLocal = $cornersLocal;

        return $this;
    }

    public function getCornersVisitor(): ?int
    {
        return $this->cornersVisitor;
    }

    public function setCornersVisitor(int $cornersVisitor): self
    {
        $this->cornersVisitor = $cornersVisitor;

        return $this;
    }

    public function getSavedLocal(): ?int
    {
        return $this->savedLocal;
    }

    public function setSavedLocal(int $savedLocal): self
    {
        $this->savedLocal = $savedLocal;

        return $this;
    }

    public function getSavedVisitor(): ?int
    {
        return $this->savedVisitor;
    }

    public function setSavedVisitor(int $savedVisitor): self
    {
        $this->savedVisitor = $savedVisitor;

        return $this;
    }

    public function getPossessionLocal(): ?int
    {
        return $this->possessionLocal;
    }

    public function setPossessionLocal(int $possessionLocal): self
    {
        $this->possessionLocal = $possessionLocal;

        return $this;
    }

    public function getPossessionVisitor(): ?int
    {
        return $this->possessionVisitor;
    }

    public function setPossessionVisitor(int $possessionVisitor): self
    {
        $this->possessionVisitor = $possessionVisitor;

        return $this;
    }

    public function getGoalShotsLocal(): ?int
    {
        return $this->goalShotsLocal;
    }

    public function setGoalShotsLocal(int $goalShotsLocal): self
    {
        $this->goalShotsLocal = $goalShotsLocal;

        return $this;
    }

    public function getGoalShotsVisitor(): ?int
    {
        return $this->goalShotsVisitor;
    }

    public function setGoalShotsVisitor(int $goalShotsVisitor): self
    {
        $this->goalShotsVisitor = $goalShotsVisitor;

        return $this;
    }

    public function getKicksToGoalLocal(): ?int
    {
        return $this->kicksToGoalLocal;
    }

    public function setKicksToGoalLocal(int $kicksToGoalLocal): self
    {
        $this->kicksToGoalLocal = $kicksToGoalLocal;

        return $this;
    }

    public function getKicksToGoalVisitor(): ?int
    {
        return $this->kicksToGoalVisitor;
    }

    public function setKicksToGoalVisitor(int $kicksToGoalVisitor): self
    {
        $this->kicksToGoalVisitor = $kicksToGoalVisitor;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
