<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CalendarsRepository")
 */
class Calendars
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teams", inversedBy="calendars")
     */
    private $teamLocal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teams", inversedBy="calendarsv")
     */
    private $teamVisitor;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $journey;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $year;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Leagues", inversedBy="calendars")
     */
    private $league;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\GamesResults", mappedBy="game", cascade={"persist", "remove"})
     */
    private $gamesResults;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GolesDate", mappedBy="game")
     */
    private $golesDates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GolesTimes", mappedBy="game")
     */
    private $golesTimes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tournament", inversedBy="calendars")
     */
    private $tournament;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cards", mappedBy="game")
     */
    private $cards;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Assists", mappedBy="game")
     */
    private $assists;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stadiums", inversedBy="calendars")
     */
    private $stadium;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StatisticsGame", mappedBy="game")
     */
    private $statisticsGames;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Lineups", mappedBy="game")
     */
    private $lineups;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FreeShots", mappedBy="game")
     */
    private $freeShots;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Penalties", mappedBy="game")
     */
    private $penalties;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Faults", mappedBy="game")
     */
    private $faults;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CornerKicks", mappedBy="game")
     */
    private $cornerKicks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Shooting", mappedBy="game")
     */
    private $shootings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Possession", mappedBy="game")
     */
    private $possessions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offsides", mappedBy="game")
     */
    private $offsides;


    public function __construct()
    {
//        $this->stadium = $this->teamLocal->getStadium()->getID();
        $this->updatedAt = new \DateTime();
        $this->createdAt = new \DateTime();
        $yeardate = date('Y');
        $this->year = (int)$yeardate;
        $this->date = new \DateTime();
        $this->golesDates = new ArrayCollection();
        $this->golesTimes = new ArrayCollection();
        $this->cards = new ArrayCollection();
        $this->assists = new ArrayCollection();
        $this->statisticsGames = new ArrayCollection();
        $this->lineups = new ArrayCollection();
        $this->freeShots = new ArrayCollection();
        $this->penalties = new ArrayCollection();
        $this->faults = new ArrayCollection();
        $this->cornerKicks = new ArrayCollection();
        $this->shootings = new ArrayCollection();
        $this->possessions = new ArrayCollection();
        $this->offsides = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeamLocal(): ?Teams
    {
        return $this->teamLocal;
    }

    public function setTeamLocal(?Teams $teamLocal): self
    {
        $this->teamLocal = $teamLocal;

        return $this;
    }

    public function getTeamVisitor(): ?Teams
    {
        return $this->teamVisitor;
    }

    public function setTeamVisitor(?Teams $teamVisitor): self
    {
        $this->teamVisitor = $teamVisitor;

        return $this;
    }

    public function getJourney(): ?int
    {
        return $this->journey;
    }

    public function setJourney(?int $journey): self
    {
        $this->journey = $journey;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getLeague(): ?Leagues
    {
        return $this->league;
    }

    public function setLeague(?Leagues $league): self
    {
        $this->league = $league;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getGamesResults(): ?GamesResults
    {
        return $this->gamesResults;
    }

    public function setGamesResults(?GamesResults $gamesResults): self
    {
        $this->gamesResults = $gamesResults;

        // set (or unset) the owning side of the relation if necessary
        $newGame = null === $gamesResults ? null : $this;
        if ($gamesResults->getGame() !== $newGame) {
            $gamesResults->setGame($newGame);
        }

        return $this;
    }


    public function __toString()
    {
        // TODO: Implement __toString() method.
        $name = $this->teamLocal." VS ".$this->teamVisitor." Jornada ".$this->journey." Año ".$this->year." Liga ".$this->league;
        return $name;
    }

    /**
     * @return Collection|GolesDate[]
     */
    public function getGolesDates(): Collection
    {
        return $this->golesDates;
    }

    public function addGolesDate(GolesDate $golesDate): self
    {
        if (!$this->golesDates->contains($golesDate)) {
            $this->golesDates[] = $golesDate;
            $golesDate->setGame($this);
        }

        return $this;
    }

    public function removeGolesDate(GolesDate $golesDate): self
    {
        if ($this->golesDates->contains($golesDate)) {
            $this->golesDates->removeElement($golesDate);
            // set the owning side to null (unless already changed)
            if ($golesDate->getGame() === $this) {
                $golesDate->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GolesTimes[]
     */
    public function getGolesTimes(): Collection
    {
        return $this->golesTimes;
    }

    public function addGolesTime(GolesTimes $golesTime): self
    {
        if (!$this->golesTimes->contains($golesTime)) {
            $this->golesTimes[] = $golesTime;
            $golesTime->setGame($this);
        }

        return $this;
    }

    public function removeGolesTime(GolesTimes $golesTime): self
    {
        if ($this->golesTimes->contains($golesTime)) {
            $this->golesTimes->removeElement($golesTime);
            // set the owning side to null (unless already changed)
            if ($golesTime->getGame() === $this) {
                $golesTime->setGame(null);
            }
        }

        return $this;
    }

    public function getTournament(): ?Tournament
    {
        return $this->tournament;
    }

    public function setTournament(?Tournament $tournament): self
    {
        $this->tournament = $tournament;

        return $this;
    }

    /**
     * @return Collection|Cards[]
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    public function addCard(Cards $card): self
    {
        if (!$this->cards->contains($card)) {
            $this->cards[] = $card;
            $card->setGame($this);
        }

        return $this;
    }

    public function removeCard(Cards $card): self
    {
        if ($this->cards->contains($card)) {
            $this->cards->removeElement($card);
            // set the owning side to null (unless already changed)
            if ($card->getGame() === $this) {
                $card->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Assists[]
     */
    public function getAssists(): Collection
    {
        return $this->assists;
    }

    public function addAssist(Assists $assist): self
    {
        if (!$this->assists->contains($assist)) {
            $this->assists[] = $assist;
            $assist->setGame($this);
        }

        return $this;
    }

    public function removeAssist(Assists $assist): self
    {
        if ($this->assists->contains($assist)) {
            $this->assists->removeElement($assist);
            // set the owning side to null (unless already changed)
            if ($assist->getGame() === $this) {
                $assist->setGame(null);
            }
        }

        return $this;
    }

    public function getStadium(): ?Stadiums
    {
        return $this->stadium;
    }

    public function setStadium(?Stadiums $stadium): self
    {
        $this->stadium = $stadium;

        return $this;
    }

    /**
     * @return Collection|StatisticsGame[]
     */
    public function getStatisticsGames(): Collection
    {
        return $this->statisticsGames;
    }

    public function addStatisticsGame(StatisticsGame $statisticsGame): self
    {
        if (!$this->statisticsGames->contains($statisticsGame)) {
            $this->statisticsGames[] = $statisticsGame;
            $statisticsGame->setGame($this);
        }

        return $this;
    }

    public function removeStatisticsGame(StatisticsGame $statisticsGame): self
    {
        if ($this->statisticsGames->contains($statisticsGame)) {
            $this->statisticsGames->removeElement($statisticsGame);
            // set the owning side to null (unless already changed)
            if ($statisticsGame->getGame() === $this) {
                $statisticsGame->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Lineups[]
     */
    public function getLineups(): Collection
    {
        return $this->lineups;
    }

    public function addLineup(Lineups $lineup): self
    {
        if (!$this->lineups->contains($lineup)) {
            $this->lineups[] = $lineup;
            $lineup->setGame($this);
        }

        return $this;
    }

    public function removeLineup(Lineups $lineup): self
    {
        if ($this->lineups->contains($lineup)) {
            $this->lineups->removeElement($lineup);
            // set the owning side to null (unless already changed)
            if ($lineup->getGame() === $this) {
                $lineup->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FreeShots[]
     */
    public function getFreeShots(): Collection
    {
        return $this->freeShots;
    }

    public function addFreeShot(FreeShots $freeShot): self
    {
        if (!$this->freeShots->contains($freeShot)) {
            $this->freeShots[] = $freeShot;
            $freeShot->setGame($this);
        }

        return $this;
    }

    public function removeFreeShot(FreeShots $freeShot): self
    {
        if ($this->freeShots->contains($freeShot)) {
            $this->freeShots->removeElement($freeShot);
            // set the owning side to null (unless already changed)
            if ($freeShot->getGame() === $this) {
                $freeShot->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Penalties[]
     */
    public function getPenalties(): Collection
    {
        return $this->penalties;
    }

    public function addPenalty(Penalties $penalty): self
    {
        if (!$this->penalties->contains($penalty)) {
            $this->penalties[] = $penalty;
            $penalty->setGame($this);
        }

        return $this;
    }

    public function removePenalty(Penalties $penalty): self
    {
        if ($this->penalties->contains($penalty)) {
            $this->penalties->removeElement($penalty);
            // set the owning side to null (unless already changed)
            if ($penalty->getGame() === $this) {
                $penalty->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Faults[]
     */
    public function getFaults(): Collection
    {
        return $this->faults;
    }

    public function addFault(Faults $fault): self
    {
        if (!$this->faults->contains($fault)) {
            $this->faults[] = $fault;
            $fault->setGame($this);
        }

        return $this;
    }

    public function removeFault(Faults $fault): self
    {
        if ($this->faults->contains($fault)) {
            $this->faults->removeElement($fault);
            // set the owning side to null (unless already changed)
            if ($fault->getGame() === $this) {
                $fault->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CornerKicks[]
     */
    public function getCornerKicks(): Collection
    {
        return $this->cornerKicks;
    }

    public function addCornerKick(CornerKicks $cornerKick): self
    {
        if (!$this->cornerKicks->contains($cornerKick)) {
            $this->cornerKicks[] = $cornerKick;
            $cornerKick->setGame($this);
        }

        return $this;
    }

    public function removeCornerKick(CornerKicks $cornerKick): self
    {
        if ($this->cornerKicks->contains($cornerKick)) {
            $this->cornerKicks->removeElement($cornerKick);
            // set the owning side to null (unless already changed)
            if ($cornerKick->getGame() === $this) {
                $cornerKick->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Shooting[]
     */
    public function getShootings(): Collection
    {
        return $this->shootings;
    }

    public function addShooting(Shooting $shooting): self
    {
        if (!$this->shootings->contains($shooting)) {
            $this->shootings[] = $shooting;
            $shooting->setGame($this);
        }

        return $this;
    }

    public function removeShooting(Shooting $shooting): self
    {
        if ($this->shootings->contains($shooting)) {
            $this->shootings->removeElement($shooting);
            // set the owning side to null (unless already changed)
            if ($shooting->getGame() === $this) {
                $shooting->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Possession[]
     */
    public function getPossessions(): Collection
    {
        return $this->possessions;
    }

    public function addPossession(Possession $possession): self
    {
        if (!$this->possessions->contains($possession)) {
            $this->possessions[] = $possession;
            $possession->setGame($this);
        }

        return $this;
    }

    public function removePossession(Possession $possession): self
    {
        if ($this->possessions->contains($possession)) {
            $this->possessions->removeElement($possession);
            // set the owning side to null (unless already changed)
            if ($possession->getGame() === $this) {
                $possession->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Offsides[]
     */
    public function getOffsides(): Collection
    {
        return $this->offsides;
    }

    public function addOffside(Offsides $offside): self
    {
        if (!$this->offsides->contains($offside)) {
            $this->offsides[] = $offside;
            $offside->setGame($this);
        }

        return $this;
    }

    public function removeOffside(Offsides $offside): self
    {
        if ($this->offsides->contains($offside)) {
            $this->offsides->removeElement($offside);
            // set the owning side to null (unless already changed)
            if ($offside->getGame() === $this) {
                $offside->setGame(null);
            }
        }

        return $this;
    }
}
