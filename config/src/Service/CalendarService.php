<?php


namespace App\Service;


use App\Entity\Calendars;
use App\Entity\VariablesLeagues;
use Doctrine\ORM\EntityManager;

class CalendarService
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function journey()
    {
        $journey = $this->em->getRepository(VariablesLeagues::class)->findOneBy(['isActive' => true]);
        $journey = $journey->getJourney();
        return $journey;
    }

    public function calendarLeaguemx()
    {
        $journey = $this->em->getRepository(VariablesLeagues::class)->findOneBy(['isActive' => true]);
        $journey = $journey->getJourney();
        $items = $this->em->getRepository(Calendars::class)->findBy(['journey' => $journey, 'league' => 1, 'year' => date('Y')], ['id' => 'DESC'], 9);
        return $items;
    }

    public function percentageToWin(int $team, int $journey, int $tournament)
    {
        $percentage=0; $goalsl=0; $goalsv=0; $itemWinLocal=0; $itemLoserLocal=0; $itemAgainLocal=0; $itemWinVisitor=0; $itemLoserVisitor=0; $itemAgainVisitor=0; $perinjuries=0;
        $queryCL = $this->em->createQuery(
            'SELECT c FROM App\Entity\Calendars c WHERE c.teamLocal='.$team.' AND c.year='.date('Y').' AND c.tournament='.$tournament
        );
        foreach ($queryCL->getResult() as $calendarL){
            $queryL = $this->em->createQuery(
                'SELECT r FROM App\Entity\GamesResults r WHERE r.game='.$calendarL->getId()
            );
            foreach ($queryL->getResult() as $resultL){
                $goalsl = $goalsl + $resultL->getMarkerLocal();
                if ($resultL->getMarkerLocal() > $resultL->getMarkerVisitor()){
                    $itemWinLocal = $itemWinLocal + 1;
                }
                if ($resultL->getMarkerLocal() < $resultL->getMarkerVisitor()){
                    $itemLoserLocal = $itemLoserLocal + 1;
                }
                if ($resultL->getMarkerLocal() == $resultL->getMarkerVisitor()){
                    $itemAgainLocal = $itemAgainLocal + 1;
                }
            }
        }

        $queryCV = $this->em->createQuery(
            'SELECT c FROM App\Entity\Calendars c WHERE c.teamVisitor='.$team.' AND c.year='.date('Y').' AND c.tournament='.$tournament
        );

        foreach ($queryCV->getResult() as $calendarV){
            $queryV = $this->em->createQuery(
                'SELECT r FROM App\Entity\GamesResults r WHERE r.game='.$calendarV->getId()
            );
            foreach ($queryV->getResult() as $resultV){
                $goalsv = $goalsv + $resultV->getMarkerVisitor();
                if ($resultV->getMarkerVisitor() > $resultV->getMarkerLocal()){
                    $itemWinVisitor = $itemWinVisitor + 1;
                }
                if ($resultV->getMarkerVisitor() < $resultV->getMarkerLocal()){
                    $itemLoserVisitor = $itemLoserVisitor + 1;
                }
                if ($resultV->getMarkerVisitor() == $resultV->getMarkerLocal()){
                    $itemAgainVisitor = $itemAgainVisitor + 1;
                }
            }
        }
        $queryP = $this->em->createQuery(
            'SELECT p FROM App\Entity\Players p WHERE p.team='.$team
        );
        foreach ($queryP->getResult() as $player){
            $queryI = $this->em->createQuery(
                'SELECT i FROM App\Entity\Injuries i WHERE i.player='.$player->getId()
            );
            foreach ($queryI->getResult() as $injurie){
                if ($injurie->getPlayer()->getPercentage() != null ){
                    $perinjuries = ($injurie->getPlayer()->getPercentage() * 20)/ 100;
                } else {
                    $perinjuries = 0;
                }
            }
        }

        $pergoals = ($goalsl + $goalsv) / $journey;
        $pergoals = ($pergoals * 7) / 3;
        $perwins = $itemWinLocal + $itemWinVisitor;
        $perwins = ($perwins * 13) / $journey;
        $perloser = $itemLoserLocal + $itemLoserVisitor;
        $perloser = ($perloser * 12) / $journey;
        $perloser = 12 - $perloser;
        if (($itemWinVisitor + $itemWinLocal) > ($itemLoserLocal + $itemLoserVisitor) ){
            $peragains = $itemAgainLocal + $itemAgainVisitor;
            $peragains = ($peragains * 10) / $journey;
            $peragains = 10 - $peragains;
        } elseif (($itemWinVisitor + $itemWinLocal) == ($itemLoserLocal + $itemLoserVisitor)){
            $peragains = $itemAgainLocal + $itemAgainVisitor;
            $peragains = ($peragains * 10) / $journey;
            $peragains = 10 - $peragains;
        } else {
            $peragains = $itemAgainLocal + $itemAgainVisitor;
            if ($peragains == 0){
                $peragains = 0;
            } else {
                $peragains = ($peragains * 10) / $journey;
            }
        }
        $pergameswl = ($itemWinLocal * 9) / $journey;
        $pergameswv = ($itemWinVisitor * 9) / $journey;
        $pergamesll = ($itemLoserLocal * 7) / $journey;
        $pergamesll = 7 - $pergamesll;
        $pergameslv = ($itemLoserVisitor * 7) / $journey;
        $pergameslv = 7 - $pergameslv;
        $pergamesal = ($itemAgainLocal * 7) / $journey;
        $pergamesal = 7 - $pergamesal;
        if (($itemWinVisitor + $itemWinLocal) > ($itemLoserLocal + $itemLoserVisitor) ){
            $pergamesav = ($itemAgainVisitor * 7) / $journey;
            $pergamesav = 7 - $pergamesav;
        } elseif (($itemWinVisitor + $itemWinLocal) == ($itemLoserLocal + $itemLoserVisitor)){
            $pergamesav = ($itemAgainVisitor * 7) / $journey;
            $pergamesav = 7 - $pergamesav;
        } else {
            $pergamesav = ($itemAgainVisitor * 7) / $journey;
            if ($pergamesav == 0){
                $pergamesav = 0;
            } else {
                $pergamesav = 7 - $pergamesav;
            }
        }


        $perefficyl = ($itemWinLocal * 8) / ( $journey / 2 );
        $perefficyl1 = ($itemWinLocal * 100) / $journey;
        $perefficyv = ($itemWinVisitor * 8) / ($journey / 2 );
        $perefficyv1 = ($itemWinVisitor * 100) / $journey;
        $percentage = $pergoals + $perwins + $perloser + $peragains + $pergameswl + $pergameswv + $pergamesll + $pergameslv + $pergamesal + $pergamesav + $perefficyl + $perefficyv;

        $percentage = $percentage - $perinjuries;
        $row = [
            'percentage' => $percentage,
            'effiV' => $perefficyv1,
            'effiL' => $perefficyl1
        ];

        return $row;
    }

}