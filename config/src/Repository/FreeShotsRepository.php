<?php

namespace App\Repository;

use App\Entity\FreeShots;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FreeShots|null find($id, $lockMode = null, $lockVersion = null)
 * @method FreeShots|null findOneBy(array $criteria, array $orderBy = null)
 * @method FreeShots[]    findAll()
 * @method FreeShots[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FreeShotsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FreeShots::class);
    }

    // /**
    //  * @return FreeShots[] Returns an array of FreeShots objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FreeShots
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
