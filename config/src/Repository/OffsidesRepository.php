<?php

namespace App\Repository;

use App\Entity\Offsides;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Offsides|null find($id, $lockMode = null, $lockVersion = null)
 * @method Offsides|null findOneBy(array $criteria, array $orderBy = null)
 * @method Offsides[]    findAll()
 * @method Offsides[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OffsidesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Offsides::class);
    }

    // /**
    //  * @return Offsides[] Returns an array of Offsides objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Offsides
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
