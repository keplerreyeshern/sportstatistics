<?php

namespace App\Repository;

use App\Entity\GolesTimes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method GolesTimes|null find($id, $lockMode = null, $lockVersion = null)
 * @method GolesTimes|null findOneBy(array $criteria, array $orderBy = null)
 * @method GolesTimes[]    findAll()
 * @method GolesTimes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GolesTimesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GolesTimes::class);
    }

    // /**
    //  * @return GolesTimes[] Returns an array of GolesTimes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GolesTimes
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
