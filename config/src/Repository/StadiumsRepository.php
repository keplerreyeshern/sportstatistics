<?php

namespace App\Repository;

use App\Entity\Stadiums;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Stadiums|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stadiums|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stadiums[]    findAll()
 * @method Stadiums[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StadiumsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stadiums::class);
    }

    // /**
    //  * @return Stadiums[] Returns an array of Stadiums objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Stadiums
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
