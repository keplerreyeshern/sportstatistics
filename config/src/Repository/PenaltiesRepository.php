<?php

namespace App\Repository;

use App\Entity\Penalties;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Penalties|null find($id, $lockMode = null, $lockVersion = null)
 * @method Penalties|null findOneBy(array $criteria, array $orderBy = null)
 * @method Penalties[]    findAll()
 * @method Penalties[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PenaltiesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Penalties::class);
    }

    // /**
    //  * @return Penalties[] Returns an array of Penalties objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Penalties
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
