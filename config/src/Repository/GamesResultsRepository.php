<?php

namespace App\Repository;

use App\Entity\GamesResults;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method GamesResults|null find($id, $lockMode = null, $lockVersion = null)
 * @method GamesResults|null findOneBy(array $criteria, array $orderBy = null)
 * @method GamesResults[]    findAll()
 * @method GamesResults[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GamesResultsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GamesResults::class);
    }

    public function backGames(int $teamLocal, int $teamVisitor)
    {
        $entityManager = $this->getEntityManager();
        $rows = []; $markerLocal=0; $markerLocal2=0; $markerVisitor=0; $markerVisitor2=0;
        $query1 = $entityManager->createQuery(
            'SELECT c FROM App\Entity\Calendars c  WHERE c.teamLocal='.$teamLocal.' AND c.teamVisitor='.$teamVisitor.' AND c.year<>'.date('Y')
        );
        foreach ($query1->getResult() as $calendar1){
            $query1G = $entityManager->createQuery(
                'SELECT g FROM App\Entity\GamesResults g  WHERE g.game='.$calendar1->getId()
            );
            foreach ($query1G->getResult() as $result1){
                $markerLocal = $result1->getMarkerLocal();
                $markerVisitor = $result1->getMarkerVisitor();
            }
            $row = [
                'id' => $calendar1->getID(),
                'teamLocal' => $calendar1->getTeamLocal()->getName(),
                'teamVisitor' => $calendar1->getTeamVisitor()->getName(),
                'teamLocalLogo' => $calendar1->getTeamLocal()->getLogo(),
                'teamVisitorLogo' => $calendar1->getTeamVisitor()->getLogo(),
                'markerLocal' => $markerLocal,
                'markerVisitor' => $markerVisitor,
                'journey' => $calendar1->getJourney(),
                'year' => $calendar1->getYear(),
                'tournament' => $calendar1->getTournament()->getName(),
            ];
            array_push($rows, $row);
        }
        $query2 = $entityManager->createQuery(
            'SELECT c FROM App\Entity\Calendars c  WHERE c.teamLocal='.$teamVisitor.' AND c.teamVisitor='.$teamLocal.' AND c.year<>'.date('Y')
        );
        foreach ($query2->getResult() as $calendar2){
            $query2G = $entityManager->createQuery(
                'SELECT g FROM App\Entity\GamesResults g  WHERE g.game='.$calendar2->getId()
            );
            foreach ($query2G->getResult() as $result2){
                $markerLocal2 = $result2->getMarkerLocal();
                $markerVisitor2 = $result2->getMarkerVisitor();
            }
            $row = [
                'id' => $calendar2->getID(),
                'teamLocal' => $calendar2->getTeamLocal()->getName(),
                'teamVisitor' => $calendar2->getTeamVisitor()->getName(),
                'teamLocalLogo' => $calendar2->getTeamLocal()->getLogo(),
                'teamVisitorLogo' => $calendar2->getTeamVisitor()->getLogo(),
                'markerLocal' => $markerLocal2,
                'markerVisitor' => $markerVisitor2,
                'journey' => $calendar2->getJourney(),
                'year' => $calendar2->getYear(),
                'tournament' => $calendar2->getTournament()->getName(),
            ];
            array_push($rows, $row);
        }

        return $rows;
    }

    public function lastResult(int $year, int $journey, int $league, int $tournament)
    {
        $entityManager = $this->getEntityManager();
        $rows = [];
        $markerLocal = 0; $markerVisitor=0; $row=[]; $item=0;
        $queryC = $entityManager->createQuery(
            'SELECT c FROM App\Entity\Calendars c  WHERE c.year='.$year.' AND c.league='.$league.' AND c.journey='.$journey.'  AND c.tournament='.$tournament.' ORDER BY c.id DESC '
        );
        $queryC->setMaxResults(1);
        foreach ($queryC->getResult() as $calendar) {
            if ($calendar->getStadium() != null){
                $stadium = $calendar->getStadium();
            } else {
                $stadium = $calendar->getTeamLocal()->getStadium();
            }
            $queryG = $entityManager->createQuery(
                'SELECT g FROM App\Entity\GamesResults g  WHERE g.game='.$calendar->getId()
            );
            foreach ($queryG->getResult() as $result){
                $markerLocal = $result->getMarkerLocal();
                $markerVisitor = $result->getMarkerVisitor();
            }
            $row = [
                'item' => $item,
                'id' => $calendar->getID(),
                'teamLocal' => $calendar->getTeamLocal()->getName(),
                'teamVisitor' => $calendar->getTeamVisitor()->getName(),
                'teamLocalLogo' => $calendar->getTeamLocal()->getLogo(),
                'teamVisitorLogo' => $calendar->getTeamVisitor()->getLogo(),
                'markerLocal' => $markerLocal,
                'markerVisitor' => $markerVisitor,
                'date' => $calendar->getDate(),
                'stadium' => $stadium,
            ];
            array_push($rows, $row);
        }
        return $rows;
    }

    public function staticstisForGame(int $gameId)
    {
        $entityManager = $this->getEntityManager();
        $rows = [];
        $possessionLocal=0; $possessionVisitor=0; $faultLocal=0; $faultVisitor=0; $cardsYellowLocal=0; $cardsRedLocal=0; $offsideLocal=0; $cornersLocal=0; $savedLocal=0; $shotLocal=0; $goalShotLocal=0;  $cardsYellowVisitor=0; $cardsRedVisitor=0; $offsideVisitor=0; $cornersVisitor=0; $savedVisitor=0; $shotVisitor=0; $goalShotVisitor=0; $assistsLocal=0; $assistsVisitor=0;
        $queryG = $entityManager->createQuery(
            'SELECT c FROM App\Entity\Calendars c  WHERE c.id='.$gameId
        );
        foreach ( $queryG->getResult() as $game ){
            $queryP = $entityManager->createQuery(
                'SELECT p FROM App\Entity\Possession p WHERE p.game='.$gameId
            );
            foreach ( $queryP->getResult() as $possession ){
                $possessionLocal = $possession->getLocal();
                $possessionVisitor = $possession->getVisitor();
            }
            $queryA = $entityManager->createQuery(
                'SELECT a FROM App\Entity\Assists a WHERE a.game='.$gameId
            );
            foreach ( $queryA->getResult() as $assists ){
                if ($game->getTeamLocal() == $assists->getTeam()){
                    $assistsLocal = $assistsLocal + 1;
                }
                if ($game->getTeamVisitor() == $assists->getTeam()){
                    $assistsVisitor = $assistsVisitor + 1;
                }
            }
            $queryf = $entityManager->createQuery(
                'SELECT f FROM App\Entity\Faults f WHERE f.game='.$gameId
            );
            foreach ( $queryf->getResult() as $faults ){
                if ($faults->getCommittedTeam() == $game->getTeamLocal()){
                    $faultLocal = $faultLocal + 1;
                }
                if ($faults->getCommittedTeam() == $game->getTeamVisitor()){
                    $faultVisitor = $faultVisitor + 1;
                }
            }
            $queryCY = $entityManager->createQuery(
                'SELECT y FROM App\Entity\Cards y WHERE y.game='.$gameId.' AND y.yellow=true'
            );
            foreach ($queryCY->getResult() as $cardYellow){
                if ($game->getTeamLocal() == $cardYellow->getPlayer()->getTeam()){
                    $cardsYellowLocal = $cardsYellowLocal + 1;
                }
                if ($game->getTeamVisitor() == $cardYellow->getPlayer()->getTeam()){
                    $cardsYellowVisitor = $cardsYellowVisitor + 1;
                }
            }
            $queryCR = $entityManager->createQuery(
                'SELECT r FROM App\Entity\Cards r WHERE r.game='.$gameId.' AND r.red=true'
            );
            foreach ($queryCR->getResult() as $cardRed){
                if ($game->getTeamLocal() == $cardRed->getPlayer()->getTeam()){
                    $cardsRedLocal = $cardsRedLocal + 1;
                }
                if ($game->getTeamVisitor() == $cardRed->getPlayer()->getTeam()){
                    $cardsRedVisitor = $cardsRedVisitor + 1;
                }
            }
            $queryOS = $entityManager->createQuery(
                'SELECT o FROM App\Entity\Offsides o WHERE o.game='.$gameId
            );
            foreach ($queryOS->getResult() as $offSide){
                if ($game->getTeamLocal() == $offSide->getTeam()){
                    $offsideLocal = $offsideLocal + 1;
                }
                if ($game->getTeamVisitor() == $offSide->getTeam()){
                    $offsideVisitor = $offsideVisitor + 1;
                }
            }
            $queryCK = $entityManager->createQuery(
                'SELECT n FROM App\Entity\CornerKicks n WHERE n.game='.$gameId
            );
            foreach ($queryCK->getResult() as $conerKicks){
                if ($game->getTeamLocal() == $conerKicks->getTeam()){
                    $cornersLocal = $cornersLocal + 1;
                    if ($conerKicks->getGoal()){
                        if ($game->getTeamLocal() == $conerKicks->getTeam()){
                            $goalShotLocal = $goalShotLocal + 1;
                            if (!$conerKicks->getAnnotated()){
                                $savedLocal = $savedLocal + 1;
                            }
                        }
                    }
                }
                if ($game->getTeamVisitor() == $conerKicks->getTeam()){
                    $cornersVisitor = $cornersVisitor + 1;
                    if ($conerKicks->getGoal()){
                        if ($game->getTeamVisitor() == $conerKicks->getTeam()){
                            $goalShotVisitor = $goalShotVisitor + 1;
                            if (!$conerKicks->getAnnotated()){
                                $savedVisitor = $assistsVisitor + 1;
                            }
                        }
                    }
                }
            }

            $queryS = $entityManager->createQuery(
                'SELECT s FROM App\Entity\Shooting s WHERE s.game='.$gameId
            );
            foreach ($queryS->getResult() as $shooting){
                if ($game->getTeamLocal() == $shooting->getTeam()){
                    $shotLocal = $shotLocal + 1;
                }
                if ($game->getTeamVisitor() == $shooting->getTeam()){
                    $shotVisitor = $shotVisitor + 1;
                }
                if ($shooting->getGoal()){
                    if ($game->getTeamLocal() == $shooting->getTeam()){
                        $goalShotLocal = $goalShotLocal + 1;
                        if (!$shooting->getAnnotated()){
                            $savedLocal = $savedLocal + 1;
                        }
                    }
                    if ($game->getTeamVisitor() == $shooting->getTeam()){
                        $goalShotVisitor = $goalShotVisitor + 1;
                        if (!$shooting->getAnnotated()){
                            $savedVisitor = $assistsVisitor + 1;
                        }
                    }
                }
            }

            $queryPP = $entityManager->createQuery(
                'SELECT p FROM App\Entity\Penalties p WHERE p.game='.$gameId
            );
            foreach ($queryPP->getResult() as $penalties){
                if ($penalties->getGoal()){
                    if ($game->getTeamLocal() == $penalties->getTeam()){
                        if (!$penalties->getAnnotated()){
                            $savedLocal = $savedLocal + 1;
                        }
                    }
                    if ($game->getTeamVisitor() == $penalties->getTeam()){
                        if (!$penalties->getAnnotated()){
                            $savedVisitor = $assistsVisitor + 1;
                        }
                    }
                }
            }

            $queryFS = $entityManager->createQuery(
                'SELECT z FROM App\Entity\FreeShots z WHERE z.game='.$gameId
            );
            foreach ($queryFS->getResult() as $freeshot){
                if ($freeshot->getGoal()){
                    if ($game->getTeamLocal() == $freeshot->getTeam()){
                        if (!$freeshot->getAnnotated()){
                            $savedLocal = $savedLocal + 1;
                        }
                    }
                    if ($game->getTeamVisitor() == $freeshot->getTeam()){
                        if (!$freeshot->getAnnotated()){
                            $savedVisitor = $assistsVisitor + 1;
                        }
                    }
                }
            }


            $row = [
                'id' => $game->getId(),
                'game' => $game,
                'teamLocal' => $game->getTeamLocal()->getName(),
                'teamVisitor' => $game->getTeamVisitor()->getName(),
                'teamLocalId' => $game->getTeamLocal(),
                'teamVisitorId' => $game->getTeamVisitor(),
                'teamLocalLogo' => $game->getTeamLocal()->getLogo(),
                'teamVisitorLogo' => $game->getTeamVisitor()->getLogo(),
                'possessionLocal' => $possessionLocal,
                'possessionVisitor' => $possessionVisitor,
                'assistsLocal' => $assistsLocal,
                'assistsVisitor' => $assistsVisitor,
                'faultsLocal' => $faultLocal,
                'faultsVisitor' => $faultVisitor,
                'cardsYellowLocal' => $cardsYellowLocal,
                'cardsYellowVisitor' => $cardsYellowVisitor,
                'cardsRedLocal' => $cardsRedLocal,
                'cardsRedVisitor' => $cardsRedVisitor,
                'offsidesLocal' => $offsideLocal,
                'offsidesVisitor' => $offsideVisitor,
                'cornersLocal' => $cornersLocal,
                'cornersVisitor' => $cornersVisitor,
                'savedLocal' => $savedLocal,
                'savedVisitor' => $savedVisitor,
                'shotLocal' => $shotLocal,
                'shotVisitor' => $shotVisitor,
                'goalShotLocal' => $goalShotLocal,
                'goalShotVisitor' => $goalShotVisitor,
            ];

            array_push($rows, $row);
        }


        return $rows;
    }

    public function offSides()
    {
        $entityManager = $this->getEntityManager();
        $rows = [];
        $queryP = $entityManager->createQuery(
            'SELECT p FROM App\Entity\Players p'
        );
        foreach ( $queryP->getResult() as $player ){
            $offsides=0;
            $queryO = $entityManager->createQuery(
                'SELECT o FROM App\Entity\Offsides o WHERE o.player='.$player->getId().'AND o.status=1'
            );
            foreach ( $queryO->getResult() as $result ){
                $offsides = $offsides + 1;
            }


            $row = [
                'id' => $offsides,
                'player' => $player->getName(),
                'team' => $player->getTeam()->getName(),
                'teamLogo' => $player->getTeam()->getLogo(),
                'position' => $player->getPosition()->getAbbreviationSPA(),
                'offsides' => $offsides,
            ];
            array_push($rows, $row);
        }


        return $rows;
    }

    public function cornersKicks()
    {
        $entityManager = $this->getEntityManager();
        $rows = [];
        $valor = null;
        $queryP = $entityManager->createQuery(
            'SELECT p FROM App\Entity\Players p'
        );
        foreach ( $queryP->getResult() as $player ){
            $corners=0;
            $queryC = $entityManager->createQuery(
                'SELECT c FROM App\Entity\CornerKicks c WHERE c.player='.$player->getId().' AND c.status=1'
            );
            foreach ( $queryC->getResult() as $result ){
                    $corners = $corners + 1;
            }


            $row = [
                'id' => $corners,
                'player' => $player->getName(),
                'team' => $player->getTeam()->getName(),
                'teamLogo' => $player->getTeam()->getLogo(),
                'position' => $player->getPosition()->getAbbreviationSPA(),
                'corners' => $corners,
            ];
            array_push($rows, $row);
        }


        return $rows;
    }

    public function scoreboard(int $tournament, int $league)
    {
        $entityManager = $this->getEntityManager();
        $rows = [];
        $queryP = $entityManager->createQuery(
            'SELECT p FROM App\Entity\Players p'
        );
        foreach ( $queryP->getResult() as $player ){
            $goales=0;
            $queryG = $entityManager->createQuery(
                'SELECT g FROM App\Entity\GolesDate g WHERE g.player='.$player->getId().' AND g.league='.$league
            );
            foreach ( $queryG->getResult() as $result ){
                if ($result->getObservations() != 'AutoGol'){
                    $goales = $goales + 1;
                }
            }


            $row = [
                'id' => $goales,
                'player' => $player->getName(),
                'team' => $player->getTeam()->getName(),
                'teamLogo' => $player->getTeam()->getLogo(),
                'position' => $player->getPosition()->getAbbreviationSPA(),
                'goales' => $goales,
            ];
            array_push($rows, $row);
        }


        return $rows;
    }

    public function statisticsForTeam(int $team, int $journey, int $tournament)
    {
        $entityManager = $this->getEntityManager();
        $rows = []; $goalsl=0; $goalsv=0; $itemWinLocal=0; $itemLoserLocal=0; $itemAgainLocal=0; $itemWinVisitor=0; $itemLoserVisitor=0; $itemAgainVisitor=0; $goalslr=0; $goalsvr=0;
        $queryCL = $entityManager->createQuery(
            'SELECT c FROM App\Entity\Calendars c WHERE c.teamLocal='.$team.' AND c.year='.date('Y').' AND c.tournament='.$tournament
        );
        foreach ($queryCL->getResult() as $calendarL){
            $queryL = $entityManager->createQuery(
                'SELECT r FROM App\Entity\GamesResults r WHERE r.game='.$calendarL->getId()
            );
            foreach ($queryL->getResult() as $resultL){
                $goalsl = $goalsl + $resultL->getMarkerLocal();
                $goalslr = $goalslr + $resultL->getMarkerVisitor();
                if ($resultL->getMarkerLocal() > $resultL->getMarkerVisitor()){
                    $itemWinLocal = $itemWinLocal + 1;
                }
                if ($resultL->getMarkerLocal() < $resultL->getMarkerVisitor()){
                    $itemLoserLocal = $itemLoserLocal + 1;
                }
                if ($resultL->getMarkerLocal() == $resultL->getMarkerVisitor()){
                    $itemAgainLocal = $itemAgainLocal + 1;
                }
            }
            $teamLocal = $calendarL->getTeamLocal();
        }

        $queryCV = $entityManager->createQuery(
            'SELECT c FROM App\Entity\Calendars c WHERE c.teamVisitor='.$team.' AND c.year='.date('Y').' AND c.tournament='.$tournament
        );

        foreach ($queryCV->getResult() as $calendarV){
            $queryV = $entityManager->createQuery(
                'SELECT r FROM App\Entity\GamesResults r WHERE r.game='.$calendarV->getId()
            );
            foreach ($queryV->getResult() as $resultV){
                $goalsv = $goalsv + $resultV->getMarkerVisitor();
                $goalsvr = $goalsvr + $resultV->getMarkerLocal();
                if ($resultV->getMarkerVisitor() > $resultV->getMarkerLocal()){
                    $itemWinVisitor = $itemWinVisitor + 1;
                }
                if ($resultV->getMarkerVisitor() < $resultV->getMarkerLocal()){
                    $itemLoserVisitor = $itemLoserVisitor + 1;
                }
                if ($resultV->getMarkerVisitor() == $resultV->getMarkerLocal()){
                    $itemAgainVisitor = $itemAgainVisitor + 1;
                }
            }
            $teamVisitor = $calendarV->getTeamVisitor();
        }
        $pergoals = ($goalsl + $goalsv) / $journey;
        $pergoals3 = ($goalslr + $goalsvr) / $journey;
        $pergoals2 = ($pergoals * 100) / 3;
        $perwins = $itemWinLocal + $itemWinVisitor;
        $perwins = ($perwins * 100) / $journey;
        $perloser = $itemLoserLocal + $itemLoserVisitor;
        $perloser = ($perloser * 100) / $journey;
        $perloser = 100 - $perloser;
        if (($itemWinVisitor + $itemWinLocal) > ($itemLoserLocal + $itemLoserVisitor) ){
            $peragains = $itemAgainLocal + $itemAgainVisitor;
            $peragains = ($peragains * 100) / $journey;
            $peragains = 100 - $peragains;
        } elseif (($itemWinVisitor + $itemWinLocal) == ($itemLoserLocal + $itemLoserVisitor)){
            $peragains = $itemAgainLocal + $itemAgainVisitor;
            $peragains = ($peragains * 100) / $journey;
            $peragains = 100 - $peragains;
        } else {
            $peragains = $itemAgainLocal + $itemAgainVisitor;
            if ($peragains == 0){
                $peragains = 0;
            } else {
                $peragains = ($peragains * 100) / $journey;
            }
        }
        $pergameswl = ($itemWinLocal * 100) / $journey;
        $pergameswv = ($itemWinVisitor * 100) / $journey;
        $pergamesll = ($itemLoserLocal * 100) / $journey;
        $pergamesll = 100 - $pergamesll;
        $pergameslv = ($itemLoserVisitor * 100) / $journey;
        $pergameslv = 100 - $pergameslv;
        $pergamesal = ($itemAgainLocal * 100) / $journey;
        $pergamesal = 100 -$pergamesal;
        if (($itemWinVisitor + $itemWinLocal) > ($itemLoserLocal + $itemLoserVisitor) ){
            $pergamesav = ($itemAgainVisitor * 100) / $journey;
            $pergamesav = 100 - $pergamesav;
        } elseif (($itemWinVisitor + $itemWinLocal) == ($itemLoserLocal + $itemLoserVisitor)){
            $pergamesav = ($itemAgainVisitor * 100) / $journey;
            $pergamesav = 100 - $pergamesav;
        } else {
            $pergamesav = ($itemAgainVisitor * 100) / $journey;
            if ($pergamesav == 0){
                $pergamesav = 0;
            } else {
                $pergamesav = 100 - $pergamesav;
            }
        }
        $perefficyl = ($itemWinLocal * 100) / $journey;
        $perefficyv = ($itemWinVisitor * 100) / $journey;

        $row = [
            'percentageGoals' => $pergoals2,
            'averageGoalsGameR' => $pergoals3,
            'averageGoalsGame' => $pergoals,
            'percentageGamesWinners' => $perwins,
            'percentageGamesLosers' => $perloser,
            'percentageGamesAgains' => $peragains,
            'percentageWinnerLocal' => $pergameswl,
            'percentageWinnerVisitor' => $pergameswv,
            'percentageLoserLocal' => $pergamesll,
            'percentageLoserVisitor' => $pergameslv,
            'percentageAgainLocal' => $pergamesal,
            'percentageAgainVisitor' => $pergamesav,
            'percentageefficencyLocal' => $perefficyl,
            'percentageefficencyVisitor' => $perefficyv,
            'total' => ($pergoals2 + $perwins + $perloser + $peragains + $pergameswl + $pergameswv + $pergamesll + $pergameslv + $pergamesal + $pergamesav + $perefficyl + $perefficyv) / 10,
            'teamVisitor' => $teamVisitor,
            'teamLocal' => $teamLocal,
        ];

//        array_push($rows, $row);



        return $row;
    }

    public function percentageToWin(int $team, int $journey, int $tournament)
    {
        $entityManager = $this->getEntityManager();
        $percentage=0; $goalsl=0; $goalsv=0; $itemWinLocal=0; $itemLoserLocal=0; $itemAgainLocal=0; $itemWinVisitor=0; $itemLoserVisitor=0; $itemAgainVisitor=0; $perinjuries=0;
        $queryCL = $entityManager->createQuery(
            'SELECT c FROM App\Entity\Calendars c WHERE c.teamLocal='.$team.' AND c.year='.date('Y').' AND c.tournament='.$tournament
        );
        foreach ($queryCL->getResult() as $calendarL){
            $queryL = $entityManager->createQuery(
                'SELECT r FROM App\Entity\GamesResults r WHERE r.game='.$calendarL->getId()
            );
            foreach ($queryL->getResult() as $resultL){
                $goalsl = $goalsl + $resultL->getMarkerLocal();
                if ($resultL->getMarkerLocal() > $resultL->getMarkerVisitor()){
                    $itemWinLocal = $itemWinLocal + 1;
                }
                if ($resultL->getMarkerLocal() < $resultL->getMarkerVisitor()){
                    $itemLoserLocal = $itemLoserLocal + 1;
                }
                if ($resultL->getMarkerLocal() == $resultL->getMarkerVisitor()){
                    $itemAgainLocal = $itemAgainLocal + 1;
                }
            }
        }

        $queryCV = $entityManager->createQuery(
            'SELECT c FROM App\Entity\Calendars c WHERE c.teamVisitor='.$team.' AND c.year='.date('Y').' AND c.tournament='.$tournament
        );

        foreach ($queryCV->getResult() as $calendarV){
            $queryV = $entityManager->createQuery(
                'SELECT r FROM App\Entity\GamesResults r WHERE r.game='.$calendarV->getId()
            );
            foreach ($queryV->getResult() as $resultV){
                $goalsv = $goalsv + $resultV->getMarkerVisitor();
                if ($resultV->getMarkerVisitor() > $resultV->getMarkerLocal()){
                    $itemWinVisitor = $itemWinVisitor + 1;
                }
                if ($resultV->getMarkerVisitor() < $resultV->getMarkerLocal()){
                    $itemLoserVisitor = $itemLoserVisitor + 1;
                }
                if ($resultV->getMarkerVisitor() == $resultV->getMarkerLocal()){
                    $itemAgainVisitor = $itemAgainVisitor + 1;
                }
            }
        }
        $queryP = $entityManager->createQuery(
            'SELECT p FROM App\Entity\Players p WHERE p.team='.$team
        );
        foreach ($queryP->getResult() as $player){
            $queryI = $entityManager->createQuery(
                'SELECT i FROM App\Entity\Injuries i WHERE i.player='.$player->getId()
            );
            foreach ($queryI->getResult() as $injurie){
                if ($injurie->getPlayer()->getPercentage() != null ){
                    $perinjuries = ($injurie->getPlayer()->getPercentage() * 20)/ 100;
                } else {
                    $perinjuries = 0;
                }
            }
        }

        $pergoals = ($goalsl + $goalsv) / $journey;
        $pergoals = ($pergoals * 7) / 3;
        $perwins = $itemWinLocal + $itemWinVisitor;
        $perwins = ($perwins * 13) / $journey;
        $perloser = $itemLoserLocal + $itemLoserVisitor;
        $perloser = ($perloser * 12) / $journey;
        $perloser = 12 - $perloser;
        if (($itemWinVisitor + $itemWinLocal) > ($itemLoserLocal + $itemLoserVisitor) ){
            $peragains = $itemAgainLocal + $itemAgainVisitor;
            $peragains = ($peragains * 10) / $journey;
            $peragains = 10 - $peragains;
        } elseif (($itemWinVisitor + $itemWinLocal) == ($itemLoserLocal + $itemLoserVisitor)){
            $peragains = $itemAgainLocal + $itemAgainVisitor;
            $peragains = ($peragains * 10) / $journey;
            $peragains = 10 - $peragains;
        } else {
            $peragains = $itemAgainLocal + $itemAgainVisitor;
            if ($peragains == 0){
                $peragains = 0;
            } else {
                $peragains = ($peragains * 10) / $journey;
            }
        }
        $pergameswl = ($itemWinLocal * 9) / $journey;
        $pergameswv = ($itemWinVisitor * 9) / $journey;
        $pergamesll = ($itemLoserLocal * 7) / $journey;
        $pergamesll = 7 - $pergamesll;
        $pergameslv = ($itemLoserVisitor * 7) / $journey;
        $pergameslv = 7 - $pergameslv;
        $pergamesal = ($itemAgainLocal * 7) / $journey;
        $pergamesal = 7 - $pergamesal;
        if (($itemWinVisitor + $itemWinLocal) > ($itemLoserLocal + $itemLoserVisitor) ){
            $pergamesav = ($itemAgainVisitor * 7) / $journey;
            $pergamesav = 7 - $pergamesav;
        } elseif (($itemWinVisitor + $itemWinLocal) == ($itemLoserLocal + $itemLoserVisitor)){
            $pergamesav = ($itemAgainVisitor * 7) / $journey;
            $pergamesav = 7 - $pergamesav;
        } else {
            $pergamesav = ($itemAgainVisitor * 7) / $journey;
            if ($pergamesav == 0){
                $pergamesav = 0;
            } else {
                $pergamesav = 7 - $pergamesav;
            }
        }


        $perefficyl = ($itemWinLocal * 8) / ( $journey / 2 );
        $perefficyl1 = ($itemWinLocal * 100) / $journey;
        $perefficyv = ($itemWinVisitor * 8) / ($journey / 2 );
        $perefficyv1 = ($itemWinVisitor * 100) / $journey;
        $percentage = $pergoals + $perwins + $perloser + $peragains + $pergameswl + $pergameswv + $pergamesll + $pergameslv + $pergamesal + $pergamesav + $perefficyl + $perefficyv;

        $percentage = $percentage - $perinjuries;
        $row = [
            'percentage' => $percentage,
            'effiV' => $perefficyv1,
            'effiL' => $perefficyl1
        ];

        return $row;
    }

    public function gamesplayed(int $tournament, int $league)
    {
        $entityManager = $this->getEntityManager();
        $rows = [];
        $factor = null;
        $queryT = $entityManager->createQuery(
            'SELECT t FROM App\Entity\Teams t WHERE t.league='.$league.' AND t.status=1'
        );
        foreach ( $queryT->getResult() as $teams ){
            $jjl=0; $jjv=0; $jwl=0; $jwv=0; $jtl=0; $jtv=0; $jll=0; $jlv=0; $gfl=0; $gfv=0; $gal=0; $gav=0; $ptsl=0; $ptsv=0;
            $queryCL = $entityManager->createQuery(
                'SELECT c FROM App\Entity\Calendars c WHERE c.teamLocal='.$teams->getId().' AND c.year='.date('Y').' AND c.tournament='.$tournament
            );
            foreach ( $queryCL->getResult() as $calendar ){
//            $jj=0; $jg=0; $je=0; $jp=0; $gf=0; $gc=0; $dg=0; $pts = 0;
                $queryL = $entityManager->createQuery(
                    'SELECT r FROM App\Entity\GamesResults r WHERE r.game='.$calendar->getId()
                );
                foreach ( $queryL->getResult() as $result ){
                    $jjl = $jjl + 1;
                    if ($result->getMarkerLocal() > $result->getMarkerVisitor() ){
                        $jwl = $jwl + 1;
                    }
                    if ($result->getMarkerLocal() > $result->getMarkerVisitor() ){
                        $ptsl = $ptsl + 3;
                    } elseif ($result->getMarkerLocal() == $result->getMarkerVisitor()){
                        $ptsl = $ptsl + 1;
                    }
                    if ($result->getMarkerLocal() == $result->getMarkerVisitor() ){
                        $jtl = $jtl + 1;
                    }
                    if ($result->getMarkerLocal() < $result->getMarkerVisitor() ){
                        $jll = $jll + 1;
                    }
                    $gfl = $gfl + $result->getMarkerLocal();
                    $gal = $gal + $result->getMarkerVisitor();

                }
            }
            $queryCV = $entityManager->createQuery(
                'SELECT c FROM App\Entity\Calendars c WHERE c.teamVisitor='.$teams->getId().' AND c.year='.date('Y').' AND c.tournament='.$tournament
            );
            foreach ( $queryCV->getResult() as $calendar ){
//            $jj=0; $jg=0; $je=0; $jp=0; $gf=0; $gc=0; $dg=0; $pts = 0;
                $queryL = $entityManager->createQuery(
                    'SELECT r FROM App\Entity\GamesResults r WHERE r.game='.$calendar->getId()
                );
                foreach ( $queryL->getResult() as $result ){
                    $jjv = $jjv + 1;
                    if ($result->getMarkerVisitor() > $result->getMarkerLocal() ){
                        $jwl = $jwl + 1;
                    }
                    if ($result->getMarkerVisitor() > $result->getMarkerLocal() ) {
                        $ptsv = $ptsv + 3;
                    } elseif ($result->getMarkerVisitor() == $result->getMarkerLocal() ) {
                        $ptsv = $ptsv + 1;
                    }
                    if ($result->getMarkerVisitor() == $result->getMarkerLocal() ) {
                        $jtv = $jtv + 1;
                    }
                    if ($result->getMarkerVisitor() < $result->getMarkerLocal() ) {
                        $jlv = $jlv + 1;
                    }
                    $gfv = $gfv + $result->getMarkerVisitor();
                    $gav = $gav + $result->getMarkerLocal();
                }
            }
            $jj = $jjl + $jjv;
            $jw = $jwl + $jwv;
            $jt = $jtl + $jtv;
            $jl = $jll + $jlv;
            $gf = $gfl + $gfv;
            $ga = $gal + $gav;
            $pts = $ptsl + $ptsv;

            $row = [
                'pts' => $pts,
                'team' => $teams->getName(),
                'teamLogo' => $teams->getLogo(),
                'games_played' => $jj,
                'games_winner' => $jw,
                'games_tied' => $jt,
                'games_loser' => $jl,
                'goals_favor' => $gf,
                'goals_against' => $ga,
                'goals_difference' => $gf - $ga,
                'points' => $pts,
            ];
            array_push($rows, $row);
        }


        return $rows;
    }

//    public function journeys(int $journey){
//        $entityManager = $this->getEntityManager();
//        $query = $entityManager->createQuery(
//            'SELECT r.marker_local, r.marker_visitor, c.team_local_id, c.team_visitor_id FROM App\Entity\GamesResults r INNER JOIN App\Entity\Calendars c  r.game_id=c.id WHERE c.journey='.$journey
//        );
//
//        return $query->getResult();
//    }

    // /**
    //  * @return GamesResults[] Returns an array of GamesResults objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GamesResults
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
