<?php

namespace App\Repository;

use App\Entity\StatisticsGame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method StatisticsGame|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatisticsGame|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatisticsGame[]    findAll()
 * @method StatisticsGame[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatisticsGameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatisticsGame::class);
    }

    // /**
    //  * @return StatisticsGame[] Returns an array of StatisticsGame objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatisticsGame
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
