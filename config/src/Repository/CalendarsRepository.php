<?php

namespace App\Repository;

use App\Entity\Calendars;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Calendars|null find($id, $lockMode = null, $lockVersion = null)
 * @method Calendars|null findOneBy(array $criteria, array $orderBy = null)
 * @method Calendars[]    findAll()
 * @method Calendars[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CalendarsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Calendars::class);
    }
    public function journeys(int $league){
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT DISTINCT(r.journey) FROM App\Entity\Calendars r WHERE r.league='.$league.' AND r.year='.date('Y')
        );

        return $query->getResult();
    }

    // /**
    //  * @return Calendars[] Returns an array of Calendars objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Calendars
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
