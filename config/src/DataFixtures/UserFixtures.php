<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
//        $user = new User();
//        $user->setUsername('Kepler');
////        $user->setName('Kepler Reyes Hernandez');
////        $user->setEmail('keplerreyeshern@gmail.com');
////        $user->setIsActive(1);
////        $user->setPhoto('sinimagen.png');
////        $user->setProfile('Super_Admin');
//        $user->setRoles(['ROLE_SUPER_ADMIN','ROLE_ADMIN', 'ROLE_USER']);
//        $user->setPassword($this->passwordEncoder->encodePassword(
//            $user,
//            'balam.2012'
//        ));
////        $user->setCreated(date_create('now'));
////        $user->setUpdated(date_create('now'));
//        $manager->persist($user);
//        $manager->flush();
        $user = new User();
        $user->setUsername('Administrador');
        $user->setName('Happy Studio Creativo');
        $user->setEmail('Admin@pontehappy.com');
        $user->setIsActive(true);
//        $user->setPhoto('sinimagen.png');
//        $user->setProfile('Super_Admin');
        $user->setRoles(['ROLE_SUPER_ADMIN','ROLE_ADMIN', 'ROLE_USER']);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'Happy.01'
        ));
        $user->setCreatedAt(date_create('now'));
        $user->setUpdatedAt(date_create('now'));
        $manager->persist($user);
        $manager->flush();
    }

    function generateRandomString($length = 10) {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }
}
