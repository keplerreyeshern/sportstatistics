<?php

namespace App\DataFixtures;

use App\Entity\Teams;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TeamsFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
//        $team = new Teams();
//
//        $team->setName('Santos Laguna');
//        $team->setLogo('santos.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('León');
//        $team->setLogo('leon.png');
//        $team->setLeague('league_mx');
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Tigres');
//        $team->setLogo('tigres.png');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Querétaro');
//        $team->setLogo('queretaro.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Necaxa');
//        $team->setLogo('necaxa.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('América');
//        $team->setLogo('america.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Monarcas Morelia');
//        $team->setLogo('morelia.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Monterrey');
//        $team->setLogo('monterrey.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Pachuca');
//        $team->setLogo('pachuca.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Guadalajara');
//        $team->setLogo('chivas.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Club Tijuana');
//        $team->setLogo('tijuana.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Cruz Azul');
//        $team->setLogo('cruzazul.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Pumas UNAM');
//        $team->setLogo('pumas.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Atlas');
//        $team->setLogo('atlas.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Atlético San Luis');
//        $team->setLogo('luis.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('FC Juárez');
//        $team->setLogo('juarez.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Toluca');
//        $team->setLogo('toluca.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Puebla');
//        $team->setLogo('puebla.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
//
//        $team = new Teams();
//
//        $team->setName('Veracruz');
//        $team->setLogo('veracruz.png');
//        $team->setLeague('league_mx');
////        $team->setCreated(date_create('now'));
////        $team->setUpdated(date_create('now'));
//        $manager->persist($team);
//        $manager->flush();
    }

    function generateRandomString($length = 10) {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }
}
